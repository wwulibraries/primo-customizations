
/* David Bass - July 2015 - homepage tabs for OneSearch */
jQuery(document).ready(function() {
	jQuery("li.tab").click(function() {
		jQuery("li").removeClass("selected");
		jQuery(this).addClass("selected");
	});


        jQuery("li.tab")
              .mouseenter(function() {
                var thisId = jQuery(this).attr("id");
                var tabtipId = "#" + thisId + "_tip";
                jQuery(tabtipId).show();
              })
              .mouseleave(function() {
                var thisId = jQuery(this).attr("id");
                var tabtipId = "#" + thisId + "_tip";
                jQuery(tabtipId).hide();
              });         

	jQuery("#submit-to-primo").on("submit", function(event) {
	 	event.preventDefault();
		searchPrimo();
	});


	jQuery("#signinLink").click(function(event) {
	 	event.preventDefault();
		searchPrimo(true);
	});


	function searchPrimo(loginPrompt) {
	 	var preSearch = "";
	 	var query = "";
	 	var searchURL = "";
	 	var domain = "http://onesearch.library.wwu.edu";

	 	if (loginPrompt) {
	  		// if the end-user clicked on the "Sign-in" link, then let's run this function, but send them to the login-prompt page first
		     	// preSearch = "https://onesearch-pds.library.wwu.edu/pds?func=load-login&institute=WWU&calling_system=primo&lang=eng&url=http://onesearch.library.wwu.edu/primo_library/libweb/action/login.do?loginFn=signin&vid=WWU&targetURL=";
		     	preSearch =  domain + "/primo_library/libweb/action/login.do?loginFn=signin&vid=WWU&targetURL=";
		  }

		  var tempQueryValue = jQuery("#search_field").val();

		  // do not submit if query is empty, unless the SignIn link was clicked
		  if ((loginPrompt != true) && (tempQueryValue == "")) {
		  	return false;
		  }

		var selectedTabScope = jQuery("li.tab.selected span").data("scope");
		var selectedTabTab = jQuery("li.tab.selected span").data("tab");

		searchURL =  domain + "/primo_library/libweb/action/dlSearch.do?highlight=true&vid=WWU&mode=Basic&search_scope=" + selectedTabScope + "&tab=" + selectedTabTab + "&institution=01ALLIANCE_WWU" ;

		if (selectedTabTab == "everythingandmore") {
			searchURL += "&pcAvailability=true";
		}


		if (tempQueryValue != "") {
			var tempQueryValueSimple = tempQueryValue.replace(/'/g,"\'");
			tempQueryValueSimple = tempQueryValueSimple.replace(/ \* /g, " ");
			tempQueryValueSimple = tempQueryValueSimple.replace(/ \? /g, " ");
			tempQueryValueSimple = tempQueryValueSimple.replace(/,/g,' ');
			var query_encoded = encodeURIComponent(tempQueryValueSimple);
			query = "&query=any,contains," + query_encoded;
		} else {
			query = "&query=";
		}

		searchURL +=  query;

		// console.log(searchURL);

		/* encoding test queries
			"'1' 2% 3! 4? 5~ 6@ 7$ 8^ 9& 10* 11( 12) 13- 14+"
			"start ~ ! @ # $ % ^ & * ( ) _ + | } { " : ? > < \ ] [ = - ` / . , ' stop"
			"start ~! @# $% ^& *( )_ +| }{ ": ?> <\ ][ =- `/ ., 'stop" ? test
			"start ~ ! @ # $ % ^ &  ( ) _ + | } { " :  > < \ ] [ = ` / .   ' stop"
		*/

		// var finalUrl =  encodeURI(preSearch + searchURL);
		var finalUrl =  preSearch + searchURL;
		location.href = finalUrl;
		return false;
	}

});


/*
document.addEventListener("DOMContentLoaded", function() {			// this is the plain Vanilla equivalent of document.ready in jQuery
	document.getElementById('signinLink').onclick = function() {
		// when the sign-in link is clicked, run the search function;
		searchPrimo(true);
	    	return false;
	};
});
*/