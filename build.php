<?php

/* 	
	author: David dot Bass @ wwu.edu
	date:	20 Jan 2016
	purpose: concatenate selected JavaScript files into one file for use in production and create a config file
	overview:
		- show list of available features/components to end-user
		- prompt them to choose which features they want
		- depending on which features the user selects, prompt the user to define necessary variables
		- upon submit, 
			- concatenate js files into one (app.js)
			- use curl to lookup scope values for their view (if scope-count is selected)
			- create config file to store user-defined variables

	TODO:
	 - only allow this form to be submitted from the current domain?
	 - convert the list of features/files to a JSON file that could be used by this script and the form in features.html
	 - provide minimized version of app.js
	 - test sanitize functions
	 - provide zipped output

*/


$today = date("D M j G:i:s T Y");               // Sat Mar 10 17:16:18 MST 2001

// list of available features 
$available_features = array(
	"dym"=>"js/did-you-mean.js",
	"ga"=>"js/analytics.js",
	"facets"=>"js/facets.js",
	"show-hide-facets" => "js/show-hide-facets.js",
	"jump"=>"js/jump-to-page.js",
	"permalink"=>"js/permalink.js",
	"scopeCount"=>"js/scope-tabs.js",
	"rap"=>"js/report-a-problem.js",
	"ui"=> array("js/jquery.shorten.js", "js/css.js", "js/ui.js", "js/header.js", "js/uncommon.js"),
	"chat" => "js/chat.js",
	"payfines" => "js/payfines.js",
	"more-options" => "js/more-options.js",
	"tooltips" => "js/tooltips.js",
	"eshelf" => "js/eshelf.js",
	"header" => "js/header.js"
);

$js_contents = '
 /* 	this file was created on ' . $today . ' 
	 see https://bitbucket.org/wwulibraries/primo-customizations for source code 
*/ 
';

$js_contents .= "\n\n" . file_get_contents("js/common.js");		// include common.js first
$scopes = "";

$config_contents = '

    var WWU_PRIMO = { ';


// which features were checked?
if (isset($_POST['selected_features'])) {
	$selected_features = $_POST['selected_features'];			// selected_features is an array

	foreach ($selected_features as $feature) {
	 	$selected_feature_filename = $available_features[$feature];
	 	if (is_array($selected_feature_filename)) {
			foreach ($selected_feature_filename as $filename) {
			 	$js_contents .= "\n\n /* @filename: " . $filename . " * --------------part of the * " . $feature . " * feature ------------------------------------------ */ ";
				$js_contents .= "\n" . file_get_contents($filename);
			}
	 	} else {
		 	$js_contents .= "\n\n /* @filename: " . $selected_feature_filename . " ---------------------------------------------- */ ";
			$js_contents .= "\n" . file_get_contents($selected_feature_filename);
	 	}
	}

} else {
	echo "<div>Please go back and select at least one feature.</div>";
	exit();
}

// if the user wants the scope-count feature, we need to populate their list of scopes using curl
//	$curl http://onesearch.library.wwu.edu/PrimoWebServices/xservice/getscopesofview?viewId=WWU
// and define the list of approvers (the domain names for Primo)
// $approved_referrers = array("onesearch.library.wwu.edu", "search.library.wwu.edu",  "alliance-primo.hosted.exlibrisgroup.com");



function sanitize_string($string) { 
	return filter_var($string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
}

function sanitize_url($string) { 
	return filter_var($string, FILTER_SANITIZE_URL);
}


// sanitize the variables and use them to populate the config file
if (isset($_POST['institution_code'])) {
	$institution_code = sanitize_string($_POST['institution_code']);
	$config_contents .= '
      institution : "' . $institution_code . '",';
} else {
	echo "<div>Please go back and define your institution code.</div>";
	exit();	
}

if (isset($_POST['vid'])) {
	$vid = sanitize_string($_POST['vid']);
} else {
	echo "<div>Please go back and define your View ID.</div>";
	exit();	
}

if (isset($_POST['file_location'])) {
	$file_location = sanitize_url($_POST['file_location']);
	$config_contents .= '
      file_host : "' . $file_location . '",		/* where are these files hosted?  */ ';
} else {
	echo "<div>Please go back and define where these files will be served from.</div>";
	exit();	
}

if (in_array("ga", $selected_features)) {
	if (isset($_POST['gac'])) {
		$gac = sanitize_string($_POST['gac']);
		$config_contents .= '
      GA_TRACKING_CODE : "' . $gac . '",';
	}

	if (isset($_POST['gad'])) {
		$gad = sanitize_url($_POST['gad']);
		$config_contents .= '
      GA_DOMAIN_NAME : "' . $gad . '",';
	}
}

if (in_array("permalink", $selected_features)) {
	// the user wants to use the permalink feature
	if (isset($_POST['goto_permalink'])) {
		$goto_permalink = sanitize_url($_POST['goto_permalink']);
		$config_contents .= '
      GOTO_PERMALINK_URL : "' . $goto_permalink . '",           /* depending on your redirector script, you may not need the question mark */ ';
	} else {
		echo "<div>Please go back and define where your permalink redirect will be located.</div>";
		exit();	
	}	
}


if (isset($_POST['error_email'])) {
	$error_email = $_POST['error_email'];
	$config_contents .= '
      error_email : "' . $error_email . '",             /* what email address should we show the end-user to ask for help?  */ ';
}

if (isset($_POST['expand_results_for'])) {
	$expand_results_for = sanitize_string($_POST['expand_results_for']);
	$config_contents .= '
      expand_results_for : "' . $expand_results_for . '",             /* list the scope-id where pcAvailability=true is enabled by default  */ ';
}



if (isset($_POST['chat_hash'])) {
	$chat_hash = sanitize_string($_POST['chat_hash']);
	$config_contents .= '
      LIBANSWERS_V2_CHAT_HASH : "' . $chat_hash . '",		/* LibAnswers Chat v2  */ ';

} else {
	echo "<div>Please go back and define your LibAnswers v2 hash.</div>";
	exit();	
}


if (in_array("scopeCount", $selected_features)) {
	// get the list of scopes needed for the scope-count.php file, and put those in a config file
	$url = "http://alliance-primo.hosted.exlibrisgroup.com/PrimoWebServices/xservice/getscopesofview?viewId=" . $vid;
	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($curlSession);
	curl_close($curlSession);

	$xml = simplexml_load_string($response); 

	foreach ($xml as $scope) {
		$scopes .= "\n" . $scope->searchLocParam ;
	}

}


$config_contents .= '
      DEV_MODE : false,
    };

';

echo '<style> #jscode , #config, #scopes { width: 99%; height: 200px; } </style>';
echo "<h2>Output</h2>";

echo "<h3>app.js</h3>";
echo "<textarea id='jscode'>" . $js_contents . "</textarea>";

echo "<h3>config.js</h3>";
echo "<textarea id='config'>" . $config_contents . "</textarea>";

echo "<h3>config.php</h3>";
echo "<textarea id='scopes'>" . $scopes . "</textarea>";

?>