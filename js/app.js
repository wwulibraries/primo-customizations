 /*     this file was created on Fri Mar 25 13:23:34 PDT 2016 
     see https://bitbucket.org/wwulibraries/primo-customizations for source code 
*/ 



// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script contains the functions needed by some or all of the components of our Primo enhancements

 "use strict";

WWU_PRIMO.common = (function() {
    
    var get_params = function(search_string) {
      var parse = function(params, pairs) {
        var pair = pairs[0];
        var parts = pair.split('=');
        var key = decodeURIComponent(parts[0]);
        var value = decodeURIComponent(parts.slice(1).join('='));

        // Handle multiple parameters of the same name
        if (typeof params[key] === "undefined") {
          params[key] = value;
        } else {
           params[key] = [].concat(params[key], value);
        }

        return pairs.length == 1 ? params : parse(params, pairs.slice(1));
      };

      // Get rid of leading ?
      return search_string.length == 0 ? {} : parse({}, search_string.substr(1).split('&'));
    };

    var params = get_params(location.search);

    return {
        "getValue" : function(fieldName) {
                var element = document.getElementById(fieldName);
                var element_value = null;
                if (element !== null) {
                    element_value = document.getElementById(fieldName).value;
                }
              var value = this.GetURLParameter(fieldName) || element_value || "";
              return value;                                    
        } ,

        "GetParameter" : function (string, sParam) {
            // thanks to http://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript/11582513#11582513
            return decodeURIComponent((new RegExp('[?|&]' + sParam + '=' + '([^&;]+?)(&|#|;|$)').exec(string)||[,""])[1].replace(/\+/g, '%20'))||'';
        } ,

        "GetURLParameter" : function (sParam) {
            // thanks to http://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript/11582513#11582513
            return decodeURIComponent((new RegExp('[?|&]' + sParam + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||'';
        } ,

        "getValues" : function (fieldName) {
            //var isPresent = location.search.indexOf("&" + fieldName + "=");
            var lookingFor = new RegExp("[\?|&]" + fieldName + "=","i");
            var isPresent = location.search.search(lookingFor);
            if (isPresent != -1) {
                var thisValue = params[fieldName];
                var returnValue = "";
                if (typeof thisValue != "string") {
                    $.each(thisValue, function(index, value) {
                        // returnValue += "&" + fieldName + "=" + value;
                        returnValue += "&" + fieldName + "=" + value.replace(/&/g, "[amp]");
                        // console.log("returnValue = " + returnValue);
                    });                
                } else {
                    returnValue += "&" + fieldName + "=" + thisValue;
                }
                return returnValue;
            } else {
                return "";
            }
        } ,

        "recreateExLiPermalink" : function (exli_permaLink) {
            // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
            // var exli_permaLink = $(".EXLButtonSendToPermalink").find("a").attr("href");
            // this should return something like
                //    "permalink.do?docId=TN_springer_jour10.2165/00003495-200161130-00006&vid=WWU&fn=permalink"

            var docId = this.GetParameter(exli_permaLink,"docId");      // extract the docId value from that string
            var vid = this.GetParameter(exli_permaLink,"vid");           // extract the vid value from the exli_permalink string
            var exli_permaLink_final = vid + ":" + docId;     // put the pieces together
            return exli_permaLink_final;
        }


    };

})();

WWU_PRIMO.fn = WWU_PRIMO.common.getValue("fn");
WWU_PRIMO.mode = WWU_PRIMO.common.getValue("mode");
WWU_PRIMO.vid = WWU_PRIMO.common.getValue("vid");

WWU_PRIMO.search_query = null;

if (WWU_PRIMO.mode == "Basic") {
    WWU_PRIMO.search_query =  WWU_PRIMO.common.getValue("search_field");
}

if (WWU_PRIMO.mode == "Advanced") {
    WWU_PRIMO.search_query = $.trim($("#input_freeText0").val());
}


 /* @filename: js/did-you-mean.js ---------------------------------------------- */ 

 WWU_PRIMO.didYouMean = function() {

    function showDidYouMeanOptions(search_query) {
        if (search_query) {
            // "were you looking for ___?"; thanks to http://smartsearch.uiowa.edu for the idea.  :)
            var search_query_lowercase = search_query.toLowerCase();
            var looking_for = "";

            var json_url = WWU_PRIMO.file_host  + "/were-you-looking-for.json?callback=?";
            // http://stackoverflow.com/questions/6849802/jquery-getjson-works-locally-but-not-cross-domain
            // http://shancarter.github.io/mr-data-converter/
           
            var getLookingForJson = $.ajax({
                'url': json_url,
                'dataType': "jsonp",
                cache : true,
                jsonpCallback: 'findMatch', // specify the callback name if you're hard-coding it
                success: function(data){
                    $.each(data, function(key, value){
                        var service_lowercase = value.service.toLowerCase();
                        var foundIt = service_lowercase.indexOf(search_query_lowercase);
                        if (foundIt !=-1) {
                            var looking_for = '<div id="lookingFor">Were you looking for <a target="_" href="' + value.URL + '">' + value.service + '</a>? </div>';
                            $("#resultsTileNoId").prepend(looking_for);
                        }
                    });                            

                }
            });
        }
    }
}();


 /* @filename: js/analytics.js ---------------------------------------------- */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script records events and sends them to Google Analytics
// NOTICE: configure the Google Analytics code and domain at the bottom of this file

 "use strict";
 
WWU_PRIMO.analytics = (function() {

    // initialize the variables used in this module;
    var a_class,  a_href,  a_text, a_action, a_id, resultsPerPage, hidden_field_id, boomValue, typeOfResult, journalTitle, src, a_title, a_title_parts, link_number;

    /* record each time the "Didn't find" button was clicked, and what the query was as a Google Analytics event */
    $('#didnt_find_link').on('click', function() {
      ga('send', 'event', 'button', 'Didnt_find_button', search_query);
    });


    /* add a counter to each results link, so that we can track which of the links was clicked (1st on page, 7th on page, etc...) */
    resultsPerPage = $('#exlidResultsTable').find("h2.EXLResultTitle").length;
    $('#exlidResultsTable').find("h2.EXLResultTitle").find("a").each(function(index) {
        var counter = index + 1;
        var resultOrderValue = "result " + counter + " of " + resultsPerPage;
        $(this).data("resultOrder", resultOrderValue);
        $(this).addClass("searchResultLink");
    });

    var openurlPage = document.location.href.toLowerCase().indexOf("/primo_library/libweb/action/openurl");
    if (openurlPage != -1) {
        // record openurl page hits (assuming the "View It" tab is selected) as a GA event (like the search results > View It links are recorded) 
        src = GetURLParameter('sid');
        hidden_field_id = "#getit1_0";
        boomValue = $(hidden_field_id).val();
        journalTitle = decodeURIComponent(WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle'));
        ga('send', 'event', 'openurl', 'journal: ' + journalTitle, 'source: ' + src);      // record the order of the link (for search results)
    }


    $("#search_field").on("blur", function() {
        var search_query = $("#search_field").val();
        if (typeof search_query !== 'undefined') {
            ga('send', 'event', 'search_query', 'blur', search_query);      // record the search_query
        }
    });

    /* track each time a link is clicked as a Google Analytics event */
    $('a').on('click', function() {
        a_class = $(this).attr('class');
        a_href = $(this).attr('href');
        a_text = $.trim($(this).text());
        a_action = "href-click";
        a_id = $(this).attr("id");

        if (a_text == "View It") {
            // this is a "View It" link on the search results page; lets append the journal title to the event title
            a_action = "View It";
            src = $("#search_field").val();

            // which link is this?
            a_title = $(this).attr('title');
            a_title_parts = a_title.split('-');    
            link_number = $.trim(a_title_parts[1]);

            typeOfResult = 'journal';

            var displayPage = document.location.href.toLowerCase().indexOf("display.do");
            if (displayPage != -1) {
                hidden_field_id = "#getit2_0";
                boomValue = $(hidden_field_id).val();
                journalTitle = WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle');
                    journalTitle = decodeURIComponent(journalTitle);                    
            } else {
                hidden_field_id = "#getitonline1_" + link_number;
                boomValue = $(hidden_field_id).val();
                typeOfResult = WWU_PRIMO.common.GetParameter(boomValue, 'O');

                if (typeOfResult.indexOf("newspaper_article") > -1) {
                    typeOfResult = "newspaper_article";
                    journalTitle = WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle');
                    journalTitle = decodeURIComponent(journalTitle);
                } else if (typeOfResult.indexOf("article") > -1) {
                    typeOfResult = "article";
                    journalTitle = WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle');
                    journalTitle = decodeURIComponent(journalTitle);
                } else if (typeOfResult.indexOf("ebook") > -1) {
                    typeOfResult = "ebook";
                    journalTitle = $(this).closest(".EXLSummary").find(".EXLResultTitle a").text();
                } else {
                    journalTitle = undefined;
                }

                if (journalTitle == "undefined") {
                    // ExLibris uses two different techniques to store the Journal Title, so this is the 2nd technique.
                    journalTitle = WWU_PRIMO.common.GetParameter(boomValue, "O8");       // please note, this is NOT zero eight; but capital oh eight.  grrr.
                        journalTitle = decodeURIComponent(journalTitle);    
                }
            }

            var eventAction = typeOfResult + ': ' + journalTitle;
            var eventLabel = 'query: ' + src;

            ga('send', 'event', 'View It', eventAction, eventLabel);      // record the order of the link (for search results)

        } else if (typeof a_class === 'undefined') {
            // this link doesn't yet have a class
            ga('send', 'event', 'link', a_action, a_text);      // record the text of the link
        } else {
            if (a_class.indexOf("searchResultLink") >= 0) {
                // this is a search result link - lets record the order, but not the title of the item
                a_action = "searchResult-order";
                a_order = $(this).data('resultOrder');
                ga('send', 'event', 'link', a_action, a_order);      // record the order of the link (for search results)
            } else {
                // this is some other type of link (probably a facet)

                if (a_class.indexOf("EXLBriefResultsPagination") >= 0) {
                    a_action = "page-number";     /// the user clicked on a "remove facet/filter" link

                    var pageNumber = parseInt(a_text);
                    if (pageNumber > 0) {
                        a_text = "goto page " + a_text;     /// the user clicked on one of the pagination numbers
                    } else {
                        a_text = "next page";     /// the user clicked on the "next page" icon
                    }
                }

                if (a_class.indexOf("EXLFirstRefinementElement") >= 0) {
                    a_action = "remove-filter";     /// the user clicked on a "remove facet/filter" link
                }

                if (a_class.indexOf("facetLink") >= 0) {
                    a_action = "apply-filter";     // the user clicked on a facet (to apply a filter)
                }

                ga('send', 'event', 'link', a_action, a_text);      // record the text of the link
            }
        }

    });

    /* CUSTOMIZATION MAY BE REQUIRED HERE */
    /* TODO: eliminate need for customization; remove hard-coded values */
    var nopacStartpage = document.location.href.indexOf("WWU_NOPAC&startpage=true");
    if (nopacStartpage == -1) {
        // only collect Google Analytics if we are NOT on the nopac start page

        // Google Analytics
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create',  WWU_PRIMO.GA_TRACKING_CODE, WWU_PRIMO.GA_DOMAIN_NAME);
        ga('set', 'anonymizeIp', true);
        ga('send', 'pageview');
    }

})();


 /* @filename: js/facets.js ---------------------------------------------- */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds checkboxes to the list of facet links, which allows for multiple facets to be applied simultaneously.
// This script also replaces the "show more" popup/ modal dialog with an expanded list of facets for each section

 "use strict";

function determineCheckboxState(cb) {
    // determine the state of a checkbox (checked, indeterminate, unchecked);
    // http://jsfiddle.net/chriscoyier/mGg85/2/
    if (cb.readOnly) {  
        cb.checked=cb.readOnly=false;        
    } else if (!cb.checked) {
        cb.readOnly=cb.indeterminate=true;
    }
}

WWU_PRIMO.facets = (function() {

    $(".EXLFacetsDisplayMore").each(function(){
        $(this).html("<button class='showHideFilters'>show/hide filters</button>");
    });

    $(".showHideFilters").click(function() {
        $(this).parent().siblings(".EXLAdditionalFacet").toggle();
        // TODO: try these:
            //    $('.element').fadeToggle('slow'); 
            //  $('.element').slideToggle('slow');
     });

    var facets = "";
    $("#facetList").each(function() {
        facets += $(this).html();
    });

    $("#exlidFacetTile").prepend("<div class='filterButtonDiv'><button type='button' tabindex='0' class='filterButton'>Apply Filter(s)</button></div>");
    $("#facetList").append("<div class='filterButtonDiv'><button type='button' tabindex='0'  class='filterButton'>Apply Filter(s)</button></div>");

    $("ol.EXLFacetsList").each(function() {
        var thisGroupIdNumber = "";
        var thisGroupId = $(this).attr("id");
        if (thisGroupId) {
            if (thisGroupId == "exlidFacetSublistX") {
                thisGroupIdNumber = 0;
            } else {
                thisGroupIdNumber = thisGroupId.replace(/\D/g,'');
                thisGroupIdNumber++;    
            }
        }

        $(this).find("li.EXLFacet a").each(function(i) {
            // put a checkbox before each facet link;
            var theURL = $(this).attr("href");
            var fctN = WWU_PRIMO.common.GetParameter(theURL,"fctN");      // extract the value from that string
            var fctV = WWU_PRIMO.common.GetParameter(theURL,"fctV");      // extract the value from that string
            var thisId = thisGroupIdNumber + "_" + i;

            // rfnIncGrp=show_only

            theURL = theURL.replace('fctN','mulIncFctN');
            theURL = theURL.replace('fctV','fctIncV');
            theURL = theURL.replace('rfnGrp','rfnIncGrp');
            theURL += "&rfnGrpCounter=" + thisGroupIdNumber;
            $(this).attr("href", theURL);       // replace the link facet querystring variables with the group equivalents

            $(this).parent().prepend("<input type='checkbox' class='facetcheckbox' tabindex='0' onclick='determineCheckboxState(this)' name='" + fctN + "' value='" + fctV + "' data-group='" + thisGroupIdNumber + "' id='facetcheckbox_" + thisId + "'> <label for='facetcheckbox_" + thisId + "'  role='checkbox' aria-checked='false' tabindex='0'  title='check once to filter by this term; check twice to exclude; and a 3rd time to un-check'></label> ");
        });
    });


    /* because  each browser renders the indeterminate state differently, let's show a minus to indicate this will be excluded */
    $(".facetcheckbox").on("click", function() {
            var isIndeterminate = $(this).prop("indeterminate");       
            var isChecked = $(this).prop("checked");

            if (isIndeterminate) {
                $(this).attr("aria-checked", "mixed");
                $(this).parent().find("a").addClass("strike").attr("title","exclude this facet from your results");
             } else if (isChecked) {
                $(this).attr("aria-checked", "true");
                $(this).parent().find("a").removeClass("strike").attr("title", "results will be filtered by this facet after you 'Apply filters'");                        
            } else {
                $(this).attr("aria-checked", "false");
                $(this).parent().find("a").removeClass("strike").attr("title", "click to filter data by this facet").after("<span class='EXLHiddenCue'>press Enter or Return to apply this facet</span>");
            }
     });

    var thisFilterValue = "";

    $(".filterButton").on("click", function() {
        // when someone clicks on the 'apply filter(s)' button, determine which checkboxes are checked, and then redirect to the new URL based on those filters

        $("li.EXLFacet input.facetcheckbox").each(function(i) {
            // append these filters to the end of the current URL and go there;
            var counter = i + 1;

            var isIndeterminate = $(this).prop("indeterminate");       
            var isChecked = $(this).prop("checked");

            var thisName = $(this).attr("name");
            var thisValue = $(this).attr("value");
            var thisGroup = $(this).data("group");

            if (isChecked) {
                thisFilterValue += "&ct=facet&mulIncFctN=" + thisName + "&fctIncV=" + thisValue;
                thisFilterValue += "&rfnIncGrp=" + thisGroup + "&rfnGrpCounter=" + thisGroup;
            }

            if (isIndeterminate) {
                thisFilterValue += "&ct=facet&mulExcFctN=" + thisName + "&fctExcV=" + thisValue;
                thisFilterValue += "&rfnExcGrp=" + thisGroup + "&rfnGrpCounter=" + thisGroup;
            }
        });

        var currentUrl = window.location.href;
        var newUrl = currentUrl.replace("initialSearch=true", "");
        newUrl = newUrl.replace("permalink_key=", "original_plink_key=");   // if someone applies facets after using a permalink, we should remove the permalink key from the url so that it doesn't appear again
        newUrl = newUrl + thisFilterValue;
        window.location = newUrl;
    });


    /*
         23 Sept 2015 - David Bass
         add an "OR" between combined .EXLRemoveRefinement, and an "AND" between each different set
         for instance, 
              Refined by: Format: Articles  Print Books             Language: English  
         would become:
              Refined by: Format: Articles OR  Print Books AND   Language: English  
    */

    var refinementCategories = $(".EXLRemoveRefinement").length - 1;
    $(".EXLRemoveRefinement").each(function(index) {
        // add "AND" between each span of this class - except for the last one
        //   console.log(index + " / " + refinementCategories);
        if (index < refinementCategories) {
            $(this).after(" AND ");
        }

        var numFacets = $(this).find("a").length;
        // console.log("numFacets = " + numFacets);

        $(this).find("a").each(function(index) {
            var theClass = $.trim($(this).attr("class"));
            // console.log("theClass = " + theClass);
            // console.log("index = " + index);
            var msg = null;

            // put "OR" in between them (by prepending "OR" for all but the first one), unless it is an exclusion, and then say " AND NOT ".
            if ((index > 0) && (numFacets > 1)) {
                // if we are not on the first facet, and there are more than one facets
                if (theClass == "EXLExcludedElement") {
                    msg = " AND ";      /* and not */
                } else {
                    msg = " OR ";
                }

               $(this).before(msg);
               $(".EXLRefinementRibbonWithExclude div a").css("border-left", "none");       // remove the left border / divider
            }

        });
    });



    var facet_count = $('#facetList').find("li.EXLFacet").find("a").length;
    $('#facetList').find("li.EXLFacet").find("a").each(function(index) {
        var facetCounter = index + 1;
        var facetOrderValue = "facet " + facetCounter + " of " + facet_count;
        $(this).data("facetOrder", facetOrderValue);
        $(this).addClass("facetLink");
        $(this).attr("title", "click to filter data by this facet");
        $(this).attr("tabindex", "0");
    });

    /* TODO: add showme tour feature to facets */
    var showFacetTour = "<span id='facetTour' class='tabtip showme'>Click once to include, twice to exclude,  and a 3rd time to clear</span>";
    $("li.EXLFacet").first().prepend(showFacetTour);

})();


 /* @filename: js/show-hide-facets.js ---------------------------------------------- */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script modifies the user interface

 "use strict";

WWU_PRIMO.showHideFacets = (function() {

    $("#exlidResultsContainer").prepend("<div id='hiddenFacetDiv'><span class='fa fa-angle-double-down'></span>   Show Facets</div>");          
         // add a hidden div that will replace the facets column when on a narrow screen

    $("#exlidFacetTile").prepend('<div id="compress-facets" class="fa fa-angle-double-left">   Hide Facets</div>');
    //    $("#exlidFacetTile").prepend('<div id="flip-facets" class="fa fa-align-right">   Flip Facets</div>');

    $(document).on("click", "#hiddenFacetDiv", function() {
        $("#hiddenFacetDiv").toggle();
        $("#exlidFacetTile").toggle();
    });

    $(document).on("click", "#compress-facets", function() {
        $("#hiddenFacetDiv").toggle();
        $("#exlidFacetTile").toggle();
    });


    // flip the facets if requested
    $(document).on("click", "#flip-facets", function() {
        $("#exlidResultsContainer").css("flex-direction", "row");
    });


})();


 /* @filename: js/jump-to-page.js ---------------------------------------------- */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds  "jump to page" functionality next to the pagination links

 "use strict";
 
WWU_PRIMO.jump = (function() {

    var searchPage = document.location.href.toLowerCase().indexOf("search.do");
    if (searchPage != -1) {
        // add a 'go to page' near the pagination links
         var  current_count = $.trim($("#resultsNumbersTile h1 em:first").text());        // global variable
        var numResults = current_count.replace(',','');
            numResults = parseInt(numResults);

        var resultsPerPage = $('#exlidResultsTable').find("h2.EXLResultTitle").length;

        var numPagesTotal = Math.round(numResults / resultsPerPage);
        var gotoPageTooltip = numPagesTotal + " pages total";

        if (numPagesTotal > 1) {
            var currentPageNumber = $("span.EXLDisplayedCount.EXLBriefResultsPaginationPageCount:eq(1)").text();
            var gotoPage = '<div class="jump2page"> jump to page <input type="text" size="3" name="gotoPage" class="gotoPageInput" value="' + currentPageNumber + '"> </div> <div class="gotoPageInputToolTip">enter the page number you want to jump to, and then press tab<br><br>' + gotoPageTooltip + ' </div> <span class="spinner">   </span> <span class="pageLoadDelay"> Sorry for the delay; this slow page load has been reported to the administrator. </span> </div>  ';
            var gotoPageBottom= '<div class="jump2page"> jump to page <input type="text" size="3" name="gotoPage" class="gotoPageInput" value="' + currentPageNumber + '"> </div>   <div class="gotoPageInputToolTip Bottom">enter the page number you want to jump to, and then press tab<br><br>' + gotoPageTooltip + ' </div><span class="spinner">   </span> <span class="pageLoadDelay"> Sorry for the delay; this slow page load has been reported to the administrator. </span> </div>  ';

            $("#resultsNavNoId").prepend(gotoPage);
            $("#resultsNavNoIdBottom").prepend(gotoPageBottom);

        }

        $("a.EXLPrevious.EXLBriefResultsPaginationLinkPrevious.EXLBriefResultsPagination").hover(
          function() {
            $(".gotoPageInputToolTipLinks").show();
          }, function() {
            $(".gotoPageInputToolTipLinks").hide();
          }
        );
        $("a.EXLPrevious.EXLBriefResultsPaginationLinkPrevious.EXLBriefResultsPagination").blur(
          function() {
            $(".gotoPageInputToolTipLinks").hide();
          }
        );


        $(".gotoPageInput").hover(
          function() {
            $(".gotoPageInputToolTip").show();
          }, function() {
            $(".gotoPageInputToolTip").hide();
          }
        );
        $(".gotoPageInput").blur(
          function() {
            $(".gotoPageInputToolTip").hide();
          }
        );


       $(document).on('change', '.gotoPageInput', function() {

            var start = new Date;
            var seconds = 0;
            setInterval(function() {
               seconds = (new Date - start) / 1000;
                if (seconds > 5) {
                    $(".pageLoadDelay").show();
                    ga('send', 'event', 'slowPageLoad', 'gotoPageNumber', search_query);
                }

            }, 1000);

            $(".spinner").show();
         
            var gotoPageNumber = $(this).val();
            var gotoIndex = (10 * (gotoPageNumber - 2)) + 1;
                gotoIndex = Math.max(0, gotoIndex);     // convert negative values to zero
            var search = location.search;
            var newSearch = null;

            if (gotoIndex === 0) {
                newSearch = search.replace("?", "ct=&pag=&indx=" + gotoIndex + "&");
            } else {
                newSearch = search.replace("?", "?ct=Next+Page&pag=nxt&indx=" + gotoIndex + "&");
            }

            location.search = newSearch;
        });
    }

})();


 /* @filename: js/permalink.js ---------------------------------------------- */ 

// Original author: david . bass @ w w u . e d u  
// enhancements by Tamara . Marnell  @ p c c . e d u 
// Purpose: this script creates a short URL (permalink)  of an otherwise long query in Primo - useful for sending via email

 "use strict";

WWU_PRIMO.permalink = (function() {

    var vid = WWU_PRIMO.vid;
    var mode = WWU_PRIMO.mode;
    var permalink_key =  WWU_PRIMO.common.GetURLParameter("permalink_key");
    var permaLinkPretty = null;
    var expand_results_for = WWU_PRIMO.expand_results_for;

  if (mode == "Advanced") {
        if (permalink_key) {
            // if this is an advanced search, submit the page after it loads because  it is not possible (yet) to store all of the query parameters in the querystring, so we have to re-submit to make it work exactly as it was originally;
            var resultsDiv = document.getElementById("resultsTileNoId");
            resultsDiv.innerHTML = "<i class='fa fa-cog fa-spin fa-2x'></i> Please wait...";
            document.getElementById("exlidFacetTile").innerHTML = "";
            // append the facets to the form inputs so they will be included in the re-submission
            convertQuerystringToHiddenFields("searchForm");
            var theForm = document.getElementById("searchForm");
            theForm.submit();
        }
    }


    function convertQuerystringToHiddenFields (formId) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)  {
            var sParameterName = sURLVariables[i].split('=');
            if ((typeof sParameterName[0] == "string") && (sParameterName[0]  != "permalink_key")) {        // do not add the permalink_key to prevent a loop
                    var thisFieldName = decodeURIComponent(sParameterName[0]);
                    var thisFieldValue = decodeURIComponent(sParameterName[1]);

                    var vlTest = /(vl\()/;
                    var vlField = vlTest.test(thisFieldName);
                    if (!vlField) {
                        var theForm = document.getElementById(formId);
                        var thisField  = document.createElement("input");
                        thisField.setAttribute("type","hidden");
                        thisField.setAttribute("name", thisFieldName);
                        thisField.setAttribute("value", thisFieldValue);
                        theForm.appendChild(thisField);        
                    }
            }
        }
    }



    // TODO: instead of removing the permalink_key for advanced searches, set some other flag that will prevent the looping problem caused by the advanced search re-submit workaround process.

    function buildDlSearchParameters() {
        // translate the current search.do parameters into dlSearch.do parameters
        // https://developers.exlibrisgroup.com/primo/apis/deeplinks/brief

        var institution = WWU_PRIMO.institution;
        var srt = WWU_PRIMO.common.getValues("srt");
        var submit = WWU_PRIMO.common.GetURLParameter('Submit');
        var tab = WWU_PRIMO.common.getValue("tab");      // sometimes the tab is not in the querystring;


        // facets
        var ct = WWU_PRIMO.common.getValues('ct');
        var fctN = WWU_PRIMO.common.getValues('fctN');
        var fctV = WWU_PRIMO.common.getValues('fctV');
        var rfnGrp = WWU_PRIMO.common.getValues('rfnGrp');
        var rfnGrpCounter = WWU_PRIMO.common.getValues('rfnGrpCounter');
        var mulIncFctN = WWU_PRIMO.common.getValues('mulIncFctN');
        var fctIncV = WWU_PRIMO.common.getValues('fctIncV');
        var rfnIncGrp = WWU_PRIMO.common.getValues('rfnIncGrp');
        var mulExcFctN = WWU_PRIMO.common.getValues('mulExcFctN');
        var fctExcV = WWU_PRIMO.common.getValues('fctExcV');
        var rfnExcGrp = WWU_PRIMO.common.getValues('rfnExcGrp');
        var rfnId = WWU_PRIMO.common.getValues('rfnId'); 
        var facets = rfnId + ct + fctN + fctV + rfnGrp + mulIncFctN + fctIncV + mulExcFctN + rfnGrpCounter + rfnIncGrp + fctExcV + rfnExcGrp;
        var query = "";
        var dlQuery0 = "";
        var dlQuery1 = "";
        var dlQuery2 = "";
        var advSearchParameters = "";
        var query_nocomma = "";

        function getAdvQuery(i) {
            var position = i;
            var position_id = i + 1;
            var scope_id = "#exlidInput_scope_" + position_id;
            var scope_name = $(scope_id).attr("name");
            var scopeValue = $(scope_id + " option:selected").val();        
            var scope = "&" + scope_name + "=" + scopeValue;        
            var precisionOperator_id = "#exlidInput_precisionOperator_" + position_id;
            var precisionOperator_name = $(precisionOperator_id).attr("name");
            var precisionOperator_value = $(precisionOperator_id + " option:selected").val();
            var precisionOperator = "&" + precisionOperator_name + "=" + precisionOperator_value;
            var boolOperator_id = "#exlidInput_boolOperator_" + position_id;
            var boolOperator_name = $(boolOperator_id).attr("name");
            var boolOperator_value = $(boolOperator_id + " option:selected").val();
            var boolOperator = "&" + boolOperator_name + "=" + boolOperator_value;
            var query_id = "#input_freeText" + position;
            var query = encodeURIComponent($.trim($(query_id).val()));
            query_nocomma = query.replace(/,/g, '+');       // replace comma with plus
            query_nocomma = query_nocomma.replace(/"/g, '%22');      // replace double quotes with %22
            query_nocomma = query_nocomma.replace(/'/g, '%27');      // replace single quotes with %27

            if (query_nocomma) {
                var value  = scope + precisionOperator + "&vl(freeText" + position + ")=" + query_nocomma + boolOperator;
                return value;                    
            } else {
                return "";
            }
        }


        if (mode === "Advanced") { 
            // advanced fields        

            // 1st query
            dlQuery0 = getAdvQuery(0);

            // 2nd query
            dlQuery1 = getAdvQuery(1);

            //  3rd query
            dlQuery2= getAdvQuery(2);

            //var operator = GetURLParameter("&vl%28boolOperator2%29");
            var pubDateName = $("#exlidInput_publicationDate_").attr("name");
            var pubDate = "&" + pubDateName + "=" + $("#exlidInput_publicationDate_ option:selected").val();        // publication date

            var mediaTypeName = $("#exlidInput_mediaType_").attr("name");
            var mediaType = "&" + mediaTypeName + "=" + $("#exlidInput_mediaType_ option:selected").val();              //  media Type

            var languageName = $("#exlidInput_language_").attr("name");
            var language = "&" + languageName + "=" + $("#exlidInput_language_ option:selected").val();               // language

            var startDayName = $("#exlidInput_drStartDay_").attr("name");
            var startDayValue = $("#exlidInput_drStartDay_ option:selected").val();
            var startDay = "&" + startDayName + "=" + startDayValue;             // start day

            var startMonthName = $("#exlidInput_drStartMonth_").attr("name");
            var startMonthValue = $("#exlidInput_drStartMonth_ option:selected").val();
            var startMonth = "&" + startMonthName + "=" + startMonthValue;           // start month

            var startYearName = $("#exlidInput_drStartMonth_ ~ input").attr("name");
            var startYearId = "#" + $("#exlidInput_drStartMonth_ ~ input").attr("id");
            var startYearValue = $(startYearId).val(); 
            var startYear = "&" + startYearName + "=" + startYearValue;            // start year
      
            var endDayName = $("#exlidInput_drEndDay_").attr("name");
            var endDayValue = $("#exlidInput_drEndDay_ option:selected").val();
            var endDay = "&" + endDayName + "=" + endDayValue;             // end day

            var endMonthName = $("#exlidInput_drEndMonth_").attr("name");
            var endMonthValue = $("#exlidInput_drEndMonth_ option:selected").val();
            var endMonth = "&" + endMonthName + "=" + endMonthValue;           // end month

            var endYearName = $("#exlidInput_drEndMonth_ ~ input").attr("name");
            var endYearId = "#" + $("#exlidInput_drEndMonth_ ~ input").attr("id");
            var endYearValue = $(endYearId).val();
            var endYear = "&" + endYearName + "=" + endYearValue;             // end year

            advSearchParameters =  pubDate + mediaType + language + startDay + startMonth + startYear + endDay + endMonth + endYear ;
        } else {
            // basic search;
            var dlScope = $(".EXLSelectedScopeId").val();
            query = encodeURIComponent($("#search_field").val());
            query_nocomma = query.replace(/,/g, '+');       // replace comma with plus
            query_nocomma = query_nocomma.replace(/"/g, '%22');      // replace double quotes with %22
            query_nocomma = query_nocomma.replace(/'/g, '%27');      // replace single quotes with %27
            dlQuery0 = "&query=any,contains," + query_nocomma + '&search_scope=' + dlScope;
        }

        var expand_results_querystring = "";
        if (tab === expand_results_for) {
            expand_results_querystring = "&pcAvailabiltyMode=true";
        }

       var dlPath =  'vid=' + vid + '&mode=' + mode + '&tab=' + tab + '&institution=' + institution +  dlQuery0 + dlQuery1 + dlQuery2 + facets + advSearchParameters + srt + expand_results_querystring;

        console.log(dlPath);

        return dlPath;
    }

     // Print permalink div
      function printPermalink(url) {
        return "<strong>Permalink:</strong> <input id='permalink-value' type='text' value='" + url + "' onclick='$(this).select();'>";
      }


    var displayPage = document.location.href.toLowerCase().indexOf("display.do");
    if (displayPage != -1) {
        (function(){
            // only run this code on the display page for now; do not run in search results page
            // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
           var  exli_permaLink_orig = $(".EXLButtonSendToPermalink").find("a").attr("href");
           var exli_permalink = WWU_PRIMO.common.recreateExLiPermalink(exli_permaLink_orig);
           var exli_permaLink_final = window.location.protocol + "//" + window.location.hostname + "/" + exli_permalink;
           var permaLink = "<span class='wwu_permalink'>" + exli_permaLink_final + "</span>";
            permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div>";
            $("#resultsListNoId").prepend(permaLinkPretty);
        }) ();
    }


    function savePermaLink(permaLink) {
        $('html, body, div, button').css("cursor", "wait");
        // var create_permalink_url = WWU_PRIMO.file_host + "permalinks/create.php";
        var create_permalink_url = "http://library.wwu.edu/goto/create.php";
        $.ajax({
            url: create_permalink_url,
            crossDomain: true,
            data: {query: permaLink},
            dataType: 'html',
            success: function(responseData, textStatus, jqXHR) {
                $('html, body, div, button').css("cursor", "auto");
                var permaLinkId = responseData;
                var newPermalink = WWU_PRIMO.GOTO_PERMALINK_URL + permaLinkId;      
                $("#wwu-permalink").html(printPermalink(newPermalink)).fadeIn(100);
            },
            error: function (responseData, textStatus, errorThrown) {
                $("#wwu-permalink").html("Could not generate permaLink.  Please email " + WWU_PRIMO.error_email + " with information on how to reproduce this problem.").fadeIn(100);
                /* TODO: notify admin */
            }
        });
    }


    // Show button on search page
      if (WWU_PRIMO.search_query != '' && RegExp(/search\.do/i).test(window.location.href) && !RegExp(/query=facet_atoz/i).test(window.location.href) && !RegExp(/fn=Browse/i).test(window.location.href) ) {
        
            var permalink_div = "<div id='wwu-permalink'>";
            
            // If these are results from a permalink, display existing key
            if (permalink_key) {
              permalink_div += printPermalink(WWU_PRIMO.GOTO_PERMALINK_URL + permalink_key);    
            } else {        
            // Otherwise display button to generate permalink
              permalink_div += "<button id='generate-permalink'>Generate a permalink for these results</button>";
            }
            permalink_div += '</div>';
            
            // Add the permaLink container to the top of the results container
            $("#resultsHeaderNoId").prepend(permalink_div);
            
            // Add click handler to generate search button
            $(document).ready(function() {
              $('#generate-permalink').click(function(e) {
                e.preventDefault();
                var advPermaLink = buildDlSearchParameters();
                savePermaLink(advPermaLink);
                return false;
              });
            });
    }


    $(window).load(function() {
            $('a').each(function() {
                // remove the permalink_key variable from any links, so that they won't show the permalink except for the first time
                var _href = $(this).attr('href');
                if (_href) {
                    var new_href = _href.replace("permalink_key=", "original_plink_key=");
                    $(this).attr('href', new_href );   
                }
            });
    });


})();


 /* @filename: js/scope-tabs.js ---------------------------------------------- */ 

// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose: this script  adds estimated result counts to the list of scopes (when they are presented as tabs)

 "use strict";

WWU_PRIMO.scopeTabs = (function() {

    var vid = WWU_PRIMO.vid;
    var mode = WWU_PRIMO.mode;
    var institution = "WWU";
    var tab_count_url = WWU_PRIMO.file_host + "/scope-count.php";

    function populateTabs() {

        function getCount(tabId, ajax_query, scope, institution) {
            ajax_query = ajax_query.replace(/%2C/g, '+');       // replace comma with plus
            var count_url = tab_count_url + "?i=" + institution + "&s=" + scope + ajax_query + "&callback=?";
            var thisId = "#" + tabId;

            $.getJSON(count_url, function(data) {
                $(thisId + " span.spin-container .spin").fadeOut("fast");
                $(thisId + " span.spin-container").addClass("fadein").text("(" + data.count + ")");
            }).done(function( json ) {
                // console.log(json); 
            }).fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            });
        }


        var current_count = 0;

        var searchPage = document.location.href.toLowerCase().indexOf("search.do");
        if (searchPage != -1) {
            current_count = $.trim($("#resultsNumbersTile h1 em:first").text());        // global variable
            var api_query = "";
            var search_query = "";

            if (mode == "Advanced") {

                var current_scope = $.trim($("#exlidSearchTabs li.selected span").attr("id"));
                current_scope = current_scope.replace("defaultScope","");

                $("[id^=input_freeText]").each(function(index) {
                    var thisCounter = index + 1;
                    var thisValue = $(this).val();
                    if (thisValue !== "") {
                        search_query = $(this).val();
                        var thisScopeId = "#exlidInput_scope_" + thisCounter;
                        var thisScope = $(thisScopeId + " option:selected").val();
                        var thisOperatorId = "#exlidInput_precisionOperator_" + thisCounter;
                        var thisOperator = $(thisOperatorId + " option:selected").val();
                        api_query += "&q[]=" + thisScope + "," + thisOperator + "," + encodeURIComponent(search_query);
                    }
                });
            } else {
                var current_scope = $.trim($(".EXLSelectedScopeId").val());
                search_query = $("#search_field").val();
                api_query = "&q=any,contains," + encodeURIComponent(search_query);
            }


            if (search_query !== "") {
                var other_scopes = {};      // create empty object

                $("#exlidSearchTabs li:visible").each(function() {
                    // for each of the visible scope tabs, insert a spinner span to indicate activity when doing the background searches
                    // wwu has an invisible tab named "default" to handle links created before we introduced tabs
                    var this_id = $(this).attr("id");
                    $(this).find("a").append(" <span class='spin-container'>     <span class='spin'>   </span> </span>");

                    var this_tab_selected = $(this).hasClass("selected");
                    if (this_tab_selected) {
                        // do not use Ajax to find count for this tab; just copy it from the html (current_count)
                        $("#" + this_id +" span.spin-container").addClass("fadein").text("(" + current_count + ")");
                    } else {
                        // add this id and the span id to an array, that we will process later
                        var this_scope_value = $(this).find("span").attr("id");                                 // what is the value of this id?
                        this_scope_value = this_scope_value.replace("defaultScope", "");        // remove the word "defaultScope" from the value before we send it to the php script
                        other_scopes[this_id] = this_scope_value;                                                       // add these values to the object, so that we can run through them later
                    }
                });

                $.each(other_scopes, function (key,value) {
                    // go through the list of other_scopes and get their result count via an ajax request
                    var tab_id = key;
                    var scope_name = value;
                    // console.log(key + " : " + value);
                    getCount(tab_id, api_query, scope_name, institution);                    
                });
            }
        }
    }


    var populateTabs = populateTabs();


    $("li.tab")
          .mouseenter(function() {
            var thisId = $(this).attr("id");
            var tabtipId = "#" + thisId + "_tabtip";
            $(tabtipId).show();
        })
          .mouseleave(function() {
            var thisId = $(this).attr("id");
            var tabtipId = "#" + thisId + "_tabtip";
            $(tabtipId).hide();
    });



    $(window).load(function() {

        // add mouseover tooltips to the scope tabs
        $("#exlidSearchTabs li.tab:visible").each(function(index) {
            var thisTitle = $(this).find("a").attr("title");
            $(this).find("a").attr("title", "");    // erase the title to prevent it from appearing

            var thisId = $(this).attr("id");
            var position = $(this).position();
            var x = position.left;
            var tabtipId = thisId + "_tabtip";
            var tooltip = "<div style='position: relative; left:" + x + "px;' id='" + tabtipId + "' class='tabtip showme'>" + thisTitle + "</div>";
            $(".EXLSearchFieldRibbonFormFields").append(tooltip);
        });
    });


})();

 /* @filename: js/report-a-problem.js ---------------------------------------------- */ 

// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose: this script  injects a "report a problem" link into the Actions menu

 "use strict";
  
WWU_PRIMO.problem = (function() {

        if ($("body").hasClass("EXLFullView")) {
            // add a "Report a problem" link to the Actions menu on the display.do page
            injectReportAProblemLink();
        }

       $(document).on("click", ".EXLTabHeaderButtonSendTo > a ", function() { 
            // add a "Report a problem" link to the Actions menu when the "Actions" link is clicked on from the search/brief results page
            var thisId = $(this).attr('id');
            var thisIdClean = thisId.replace(/[^\w\s]/gi, '');           // remove special characters
            $(this).parent("li").attr("id", thisIdClean);
            injectReportAProblemLink(thisIdClean);
        });

       function injectReportAProblemLink (thisIdClean) {        
            if (typeof thisIdClean === "undefined") {
                // we are on the display.do page; find the only Actions menu;
                var thisId = $("li.EXLTabHeaderButtonSendTo a").attr("id");
                if (thisId) {
                    var thisIdClean = thisId.replace(/[^\w\s]/gi, '');           // remove special characters
                }
            }

            // this was called from the brief/search results page
            var $child_list = $("#" + thisIdClean).parent().find("ol.EXLTabHeaderButtonSendToList, ol.EXLTabHeaderButtonSendTo");
            var child_list_a = $child_list.find(".EXLButtonSendToPermalink a, .EXLButtonSendToOpenUrl a").attr('href');

            // find the href attribute for the exli permalink
            var exli_permalink_generated = WWU_PRIMO.common.recreateExLiPermalink(child_list_a);
            
            // send that value to the recreate function and store it as this variable
            var report_a_problem_url = "http://library.wwu.edu/content/4769?permalink_path=" + exli_permalink_generated;

            // append the generated permalink to our target url, but only if it does not already have one
            var reportaproblem_id = "report-a-problem-" + thisIdClean;
            if($("#" + reportaproblem_id).length == 0) {
                // only create this if it does not already exist
                $child_list.append("<li id='" + reportaproblem_id + "'><a target='_blank' href='" + report_a_problem_url + "'> Report a Problem <span class='rap-bug fa fa-bug'></span></a> </li>");
            }

       }

})();


 /* @filename: js/jquery.shorten.js * --------------part of the * ui * feature ------------------------------------------ */ 
/*
 * jQuery Shorten plugin 1.1.0
 *
 * Copyright (c) 2014 Viral Patel
 * http://viralpatel.net
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */

/*
** updated by Jeff Richardson
** Updated to use strict,
** IE 7 has a "bug" It is returning underfined when trying to reference string characters in this format
** content[i]. IE 7 allows content.charAt(i) This works fine in all modern browsers.
** I've also added brackets where they werent added just for readability (mostly for me).
*/

(function($) {
    $.fn.shorten = function(settings) {
        "use strict";

        var config = {
            showChars: 100,
            ellipsesText: "...",
            moreText: "more",
            lessText: "less",
            errMsg: null,
            force: false
        };

        if (settings) {
            $.extend(config, settings);
        }

        if ($(this).data('jquery.shorten') && !config.force) {
            return false;
        }
        $(this).data('jquery.shorten', true);

        $(document).off("click", '.morelink');

        $(document).on({
            click: function() {

                var $this = $(this);
                if ($this.hasClass('less')) {
                    $this.removeClass('less');
                    $this.html(config.moreText);
                    $this.parent().prev().prev().show(); // shortcontent
                    $this.parent().prev().hide(); // allcontent

                } else {
                    $this.addClass('less');
                    $this.html(config.lessText);
                    $this.parent().prev().prev().hide(); // shortcontent
                    $this.parent().prev().show(); // allcontent
                }
                return false;
            }
        }, '.morelink');

        return this.each(function() {
            var $this = $(this);

            var content = $this.html();
            var contentlen = $this.text().length;
            if (contentlen > config.showChars) {
                var c = content.substr(0, config.showChars);
                if (c.indexOf('<') >= 0) // If there's HTML don't want to cut it
                {
                    var inTag = false; // I'm in a tag?
                    var bag = ''; // Put the characters to be shown here
                    var countChars = 0; // Current bag size
                    var openTags = []; // Stack for opened tags, so I can close them later
                    var tagName = null;

                    for (var i = 0, r = 0; r <= config.showChars; i++) {
                        if (content[i] == '<' && !inTag) {
                            inTag = true;

                            // This could be "tag" or "/tag"
                            tagName = content.substring(i + 1, content.indexOf('>', i));

                            // If its a closing tag
                            if (tagName[0] == '/') {


                                if (tagName != '/' + openTags[0]) {
                                    config.errMsg = 'ERROR en HTML: the top of the stack should be the tag that closes';
                                } else {
                                    openTags.shift(); // Pops the last tag from the open tag stack (the tag is closed in the retult HTML!)
                                }

                            } else {
                                // There are some nasty tags that don't have a close tag like <br/>
                                if (tagName.toLowerCase() != 'br') {
                                    openTags.unshift(tagName); // Add to start the name of the tag that opens
                                }
                            }
                        }
                        if (inTag && content[i] == '>') {
                            inTag = false;
                        }

                        if (inTag) { bag += content.charAt(i); } // Add tag name chars to the result
                        else {
                            r++;
                            if (countChars <= config.showChars) {
                                bag += content.charAt(i); // Fix to ie 7 not allowing you to reference string characters using the []
                                countChars++;
                            } else // Now I have the characters needed
                            {
                                if (openTags.length > 0) // I have unclosed tags
                                {
                                    //console.log('They were open tags');
                                    //console.log(openTags);
                                    for (j = 0; j < openTags.length; j++) {
                                        //console.log('Cierro tag ' + openTags[j]);
                                        bag += '</' + openTags[j] + '>'; // Close all tags that were opened

                                        // You could shift the tag from the stack to check if you end with an empty stack, that means you have closed all open tags
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    c = $('<div/>').html(bag + '<span class="ellip">' + config.ellipsesText + '</span>').html();
                }else{
                    c+=config.ellipsesText;
                }

                var html = '<div class="shortcontent">' + c +
                    '</div><div class="allcontent">' + content +
                    '</div><span><a href="javascript://nop/" class="morelink">' + config.moreText + '</a></span>';

                $this.html(html);
                $this.find(".allcontent").hide(); // Hide all text
                $('.shortcontent p:last', $this).css('margin-bottom', 0); //Remove bottom margin on last paragraph as it's likely shortened
            }
        });

    };

})(jQuery);


 /* @filename: js/css.js * --------------part of the * ui * feature ------------------------------------------ */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds stylesheets for different modes

 "use strict";
 
WWU_PRIMO.css = (function() {

    var mode = WWU_PRIMO.mode;
    var fn = WWU_PRIMO.fn;
    var lnk = null;

    if (mode == "Advanced" ) {
        lnk = document.createElement('link');
        lnk.type='text/css';
        lnk.href = WWU_PRIMO.file_host + '/wwu/css/advanced.css';
        lnk.rel='stylesheet';
        document.getElementsByTagName('body')[0].appendChild(lnk);            
    }

    var looking_for = new RegExp('^Browse');
    var mode_browse = looking_for.test(mode);
    var fn_browse = looking_for.test(fn);

    if  (mode_browse || fn_browse) {
        lnk = document.createElement('link');
        lnk.type='text/css';
        lnk.href = WWU_PRIMO.file_host + '/wwu/css/browse.css';
        lnk.rel='stylesheet';
        document.getElementsByTagName('body')[0].appendChild(lnk);       

    }

    if  ((fn == "almaAzSearch") || (fn == "goAlmaAz")) {
        lnk = document.createElement('link');
        lnk.type='text/css';
        lnk.href = WWU_PRIMO.file_host + '/wwu/css/az.css';
        lnk.rel='stylesheet';
        document.getElementsByTagName('body')[0].appendChild(lnk);            
    }

})();


 /* @filename: js/ui.js * --------------part of the * ui * feature ------------------------------------------ */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script modifies the user interface

 "use strict";

WWU_PRIMO.ui = (function() {

    var fn = WWU_PRIMO.fn;
    var mode = WWU_PRIMO.mode;
    var vid = WWU_PRIMO.vid;

   $(window).scrollTop(0);

    // replace the link on error.do with a different link;
    /* TODO: make this customizable */
    $("#exlidErrorSubMessage").html("If the problem persists, please <a href='http://askus.library.wwu.edu/browse.php?tid=23567'>contact us</a>.");

    var searchPage = document.location.href.toLowerCase().indexOf("search.do");
    var newSearchUrl =  $("#btn_new_search").attr("href");    
    $("#onesearch-logo-link").attr("href", newSearchUrl);
        // TODO: add the vid to the end of this url

    $(".EXLReviewsTab a").text("Save/Share");

     $(".EXLAvailabilityCallNumber").hide();     // we will show these after we remove the parens on window load

    if (mode == "Basic") {
        var search_query = WWU_PRIMO.common.getValue("search_field");

        if (searchPage != -1) {
            $("#exlidFacetTile").addClass("basic");
            $("#resultsTileNoId").addClass("basic");
        }    

        // magnifying glass 
        $("div.EXLSearchFieldRibbonFormSubmitSearch").html("<button id='goButton' type='submit'  title='basic search' value='' class='button box-shadow-outset'>  </button>");

        $(".EXLSearchFieldRibbon").append(" <div id='advsearch'><a id='advsearch_href_simple'>Advanced Search</a></div>");   

        $("#advsearch_href_simple").on("click", function() { 
            // append the query to the adv_search button
            var originalAdvSearchUrl = $("#btn_adv_search").attr('href');
            var newestQuery = $("#search_field").val();
            var newAdvSearchUrl = originalAdvSearchUrl.replace(/(vl\(freeText0\)=).*?(&)/,'$1' + newestQuery + '$2');
            window.location = newAdvSearchUrl;
        })
    } 

    if (mode == "Advanced") {
        $("#exlidAdvancedSearchRibbon #goButton").val("Search").attr("title","advanced search"); 
               var img = WWU_PRIMO.file_host + "/images/onesearch-magnifying-glass.png";
        $("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormLinks a").html("<img alt='magnifying glass icon' src='" + img + "' /> Basic Search");
        $("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormLinks a").attr("title","Basic Search");
        $("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormClearSearch input").attr("value","").attr("title","clear search");
        // $("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormClearSearch").append("clear");

        var query0 = $.trim($("#input_freeText0").val());
        if (searchPage != -1) {
            if (query0 == "") {
                // show the infographic  if there is no query
                $("#exlidFacetTile").hide();
                                            var img = WWU_PRIMO.file_host  + "/images/onesearch-scopes-june-2015.png";
                $("div#exlidHeaderSystemFeedback").prepend("<img id='scopesOverview'  alt='overview of search scopes' src='" + img + "' />");
            }
        }
    }

   var looking_for = new RegExp('^Browse');
    var mode_browse = looking_for.test(mode);
    var fn_browse = looking_for.test(fn);

    if  (mode_browse || fn_browse) {
           $("#goButton").val("");      // remove the text from the button
           $(".EXLSearchFieldRibbonFormLinks  a").text("Basic Search");
    }

    if  ((fn == "almaAzSearch") || (fn == "goAlmaAz")) {
           $("#goButton").val("");      // remove the text from the button
    }
    
   $(".EXLClearSimpleSearchBoxButtonClose").css("background-image","none !important");


    // prep the tabs 
    $("div.EXLSearchTabsContainer").addClass("tab-row");
    $("ul#exlidSearchTabs").addClass("tabrow");
    $("li.EXLSearchTab").addClass("tab");
    $("li.EXLSearchTabSelected").addClass("selected");

    // My Account / e-Shelf
    $("div.EXLMyAccountTabsRibbon").addClass("tab-row");
    $("ul.EXLMyAccountTabs").addClass("tabrow");
    $("li.EXLMyAccountTab").addClass("tab");
    $("li.EXLMyAccountSelectedTab").addClass("selected");

    $(".EXLResultTabs").children("li").children("a").addClass("tab-button");      // add a new className to the links to make them look like buttons

    $("li[id*='-ReviewsTab']").hide();
    $("li[id*='-LocationsTab']").hide();
    $("li[id*='-RecommendTab']").hide();

    // $(".EXLTabsRibbon div li").addClass("tab").addClass("tooltip");
    $(".EXLTabsRibbon div li.EXLResultSelectedTab").addClass("selected");

    var displayPage = document.location.href.toLowerCase().indexOf("display.do");
    if (displayPage != -1) {
        // select the Details tab on the display.do page
        $("li[id*='DetailsTab']").addClass("selected");
        $("#Description-1").addClass("shorten");

        // clone the 'back to results' link and add it to the top of the page
        $(".EXLBackToResults").clone().addClass("top").appendTo( "#exlidSearchRibbon" );
    }

    $("#search_field").attr("placeholder","\"Use quotation marks for exact match.\"");


    $(document).on("click", ".email-export", function() { 
        /* find the closest actions button and click it */
        var thisId = $(this).attr("id");
        var str = "$('#" + thisId + "').parents('.EXLResult').find('.EXLTabHeaderButtonSendToList a').click();";
        $(thisId).parents('.EXLResult').find(".EXLTabHeaderButtonSendTo a").click();
    });

    if (searchPage != -1) {
        if  (mode_browse || fn_browse) {
            $("#exlidResultsContainer").addClass("browse");
        }

        $("#exlidClearSearchBox").on("click", function() {
            $("#search_field").focus();     // set the focus to the search field when someone clicks the clear-search button
        });


        /* move the thumbnail to the title column */
        $('td.EXLThumbnail').each(function() {
            var thumbnailHtml = "<div class='thumbnailHtml'>" + $(this).html() + "</div>";
            $(this).parent().find(".EXLResultAvailability").prepend(thumbnailHtml);
            $(this).remove();
        });
        
        /* move the eShelf star over to the left of the item title */
        $("td.EXLMyShelfStar").each(function() {
                var thisContents = $(this).html();
                $(this).parent().find("h2.EXLResultTitle").prepend(thisContents);
                $(this).remove();
        });


        if ((fn == "almaAzSearch") || (fn == "goAlmaAz")) {
            $("#exlidSearchRibbon").addClass("az");
            $("#permalinkPretty").hide();
        }
    }

    if (search_query !== "") {
        // append the query to the adv_search button
        $('#btn_adv_search').attr('href', function() {
            return this.href + '&vid=' + vid + '&vl(freeText0)=' + search_query + "&";
        });
    }


    $("#goButton").hover(
      function() {
        $("#basic-text").show();
      }, function() {
        $("#basic-text").hide();
      }
    );

    $("#advsearch_btn_simple").hover(
      function() {
        $("#adv-text").show();
      }, function() {
        $("#adv-text").hide();
      }
    );

     $(".shorten").shorten( {
            showChars: '500',
            moreText: 'read more',
            lessText: 'read less'
    });



    $("#submit-feedback").click(function() {
        thisObjectId = $(this).data("objectId");
        theComment = $("#feedback-text").val();
        addComment(thisObjectId, theComment);
    });

    reportAddPageToEshelfClick = (function() {
        // add a tick mark when to the  "Add page to e-Shelf" link when clicked
        var cached_function = reportAddPageToEshelfClick;

        return function(str) {
            cached_function.apply(this, arguments); // use .apply() to call it
            $(".EXLFacetSaveToEShelfAction").append("<i class='fa fa-check'></i>");
        };
    }());


    $(window).load(function() {
        $(".EXLAvailabilityCallNumber").each(function() {
            // remove parens from EXLAvailabilityCallNumber 
            var newText = $(this).text().replace(/\(|\)/g, '');
            $(this).text(newText);
            // inherit the status color of the parent 
            var parentClass = $(this).parent(".EXLResultStatusAvailable").attr("class");
            $(this).addClass(parentClass);
        }).fadeIn(500);     
    });

})();


 /* @filename: js/header.js * --------------part of the * ui * feature ------------------------------------------ */ 

// Author: david.bass @ w w u . e d u  
// Date: November 2015
// Purpose: this script replaces the standard header with a modified version (that is keyboard accessible - thanks to Kate Deibel  @ UW.edu and  Terrill Thompson for their help with this)

 "use strict";

WWU_PRIMO.header = (function() {

    var vid = WWU_PRIMO.vid;
    var mode = WWU_PRIMO.mode;

    var search_query = "";
    if (mode == "Basic") {
        search_query =  WWU_PRIMO.common.getValue("search_field");
    }

    if (mode == "Advanced") {
        search_query = $.trim($("#input_freeText0").val());
    }

    var menu_script = WWU_PRIMO.file_host + 'js/menubar.js';

    $.getScript( menu_script , function(data, textStatus){
        var menu1 = new menubar('kba_nav', false);
    });

    var UserTile = $("#exlidUserAreaTile").html();      // copy the contents of this into memory
    $("#newMenu").prependTo("body");        // move the newMenu to the top of the body

    var loginLogoutLink = "";
    var loginLogoutTitle = "";
    var targetURL = encodeURIComponent(document.location.href);

     var userName = $.trim($('.EXLUserNameDisplay').text());        // copy the user's name into memory;

    if (userName !== "Guest") {
        var hasComma = userName.toLowerCase().indexOf(",");      // a Guest will not have a comma
        if (hasComma != -1) {
            userName = userName.split(',');     // split the name at the comma
            userName = userName[1];             // use what follows that
            userName = userName.split(' ');     // split the name at the first space
            userName = userName[1];             // use what follows
        }

        userName = "Hello " + userName + " :)";
        loginLogoutLink = "/primo_library/libweb/action/logout.do?loginFn=signout&vid=" + vid + "&targetURL=" + targetURL;

        loginLogoutTitle = "Logout";
        $("#myAccountMenuHeading").html(userName);
        $("#loginLink").addClass("EXLSignOut");

    } else {
        userName = "";
        loginLogoutLink = "/primo_library/libweb/action/login.do?loginFn=signin&initializeIndex%3dtrue&vid=" + vid + "&targetURL=" + targetURL;

        loginLogoutTitle = "Sign-in";
        $("#myAccountMenuHeading").html("<a id='loginLinkMenu' href='" + loginLogoutLink + "'>Sign-in</a>");
    }

    $("#loginLink").attr("href", loginLogoutLink);
    $("#loginLink").attr("title", loginLogoutTitle);
    $("#loginLink-tooltip").text(loginLogoutTitle);

    $("span#username").html(userName);


    if (search_query == "") {
        var worldcat_url = "http://www.worldcat.org";
        $("#Worldcat-Menu-Link").attr("href", worldcat_url);
    } else {
       var worldcat_url = "http://www.worldcat.org/search?qt=worldcat_org_all&q=" + search_query;
        $("#Worldcat-Menu-Link").attr("href", worldcat_url);
     }


    $("a.submenuitem.exlibris").each(function() {
        // append the vid to each ExLibris url in the new menu
        var thisHref = $(this).attr("href");
        var newHref = thisHref + "&vid=" + vid;
        $(this).attr("href", newHref);
    });

})();


 /* @filename: js/uncommon.js * --------------part of the * ui * feature ------------------------------------------ */ 

// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose:: this script includes workarounds or features that are most likely only useful at wwu.edu - not recommended unless you need them.

 "use strict";
 
WWU_PRIMO.uncommon= (function() {

    // 21 July 2015 - we have a hidden 'default' tab to accommodate  links to that previously-used tab name (pre-tab-scopes/July 2015)
    // if the tab value equals 'default', then we should change it to 'everything' to start phasing it out for future permalinks and links
    var tab =  WWU_PRIMO.common.getValue("tab");      // sometimes the tab is not in the querystring;
    if (tab == "default") {
        $("#tab").val("everything");
        $("#exlidTab2").addClass("selected");
    }

})();


 /* @filename: js/chat.js ---------------------------------------------- */ 

// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds the Springshare chat functionality to Primo

 "use strict";

    $(window).load(function() {
        /* 25aug2014 - David Bass - have to clone this for use in the "Didn't find" popup */
        //  $("#libchat_btn_widget").clone().appendTo("#chat_with_librarian");
        $("#exlidSearchBanner").fadeIn();
        $("#libchat_btn_widget a").attr("target", "_blank");

    });


WWU_PRIMO.chat = function() {

       /* LibGuides Version 2 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

       if (WWU_PRIMO.LIBANSWERS_V2_CHAT_HASH) {
          var chat_html = "<div id='chatDiv'><div id='chatDivLinkContainer'><div id='libchat_117f795f06117e9f4be68a41d4c52c60'></div></div></div>";

          if (document.getElementById("exlidSearchBanner")) {
            $("#exlidSearchBanner").html(chat_html);
          }

          if (document.getElementsByClassName("EXLSearchFieldRibbon")) {
            $(".EXLSearchFieldRibbon").append(chat_html);
          }

        var libguide_script_url = '//v2.libanswers.com/load_chat.php?hash=' +  WWU_PRIMO.LIBANSWERS_V2_CHAT_HASH;        
         loadjscssfile(libguide_script_url, "js");

       }

    /* LibGuides Version 1 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

       if (WWU_PRIMO.LIBGUIDES_V1_CHAT_HASH) {
            if (document.getElementById("exlidSearchBanner")) {
                document.getElementById("exlidSearchBanner").innerHTML = "<div id='chatDiv'><div id='chatDivLinkContainer'><div id='libchat_btn_widget'></div></div></div>";
            }

            if ($("#exlidAdvancedSearchTile")) {
               $("#exlidAdvancedSearchTile").append("<div id='chatDiv'><div id='chatDivLinkContainer'><div id='libchat_btn_widget'></div></div></div>");
            }


            /* libchat_btn needs to be in the global scope */
            var libchat_btn = {
                iid: "123",
                key: "see your libguides account for this",
                domain: "askus.library.example.edu",
                splash: "Welcome to Example Libraries Chat!",
                wait: "Please wait... A librarian will connect shortly!",
                error_offline: "Sorry it doesn't appear any librarians are online... Please try again later.",
                button_online: "//library.example.edu/images/chat-online.png",
                button_offline: "//library.example.edu/images/chat-offline.png",
                question: "Please enter question below:",
                star_ratings: true,
                star_text: "Please rate this chat:",
                depart_id: "987"
                };


            $.getScript('//libanswers.com/js/chat_load_client.js', function(data, textStatus){
                // console.log("chat client loaded");
            });
        }    
}();


 /* @filename: js/eshelf.js ---------------------------------------------- */ 
// david . bass @ w w u  . e d u
// March 2016
// show which items have been deleted in the e-shelf

 "use strict";

WWU_PRIMO.eshelf = (function() {



    var vid = WWU_PRIMO.vid;

      if ((RegExp(/basket\.do/i).test(window.location.href))) {
    // loop through each of the user's e-shelf items and note which ones have been deleted

        // if the eshelf is loaded in an iframe, break out of the iframe 
        if (top !== self) top.location.href = self.location.href;


        var deleted_msg = "The record has been deleted from the database";
        var num_deleted = 0;

        // go through each cell and see if it contains the deleted_msg;
        $("table.itemsListTable td.type").each(function() {
            var this_td = $(this).attr('onclick');
            // console.log(this_td);
            if (this_td.indexOf(deleted_msg) > -1) {
                num_deleted++;
                $(this).addClass("strike").css("color","red");
                // $(this).prev().find("input:checkbox").attr('checked', true); // check these by default, to make them easier to delete/remove;

                var author = $(this).siblings(".author").text();
                var title = $(this).siblings(".title").text();

                $(this).siblings( ).not(".itemsList_left").attr("onclick", "").addClass("newSearch");
                $(this).parent("tr").data({"author": author, "title": title});

            }   
        });

        $(".newSearch").on("click", function() {
            var $parent = $(this).parent();
            $("tr").removeClass("selected");
            $parent.addClass("selected");
            var author = $parent.data("author");
            var title = $parent.data("title");
            var newSearchUrl = "/primo_library/libweb/action/dlSearch.do?embed=true&institution=01ALLIANCE_WWU&mode=Advanced&vl(freeText0)=" + author + "&query=creator,exact," + author + "&vl(freeText1)=" + title + "&query=title,exact," + title + "&vid=" + vid + "&search_scope=All&tab=everything";
            $("#demoLibId").attr("src", newSearchUrl);

        });

        if (num_deleted > 0) {
            // add a message at the top telling the user how many have been deleted, and to prompt them to remove them;
            var msg = num_deleted + " of the items in your e-shelf no longer exist and have been marked in <span class='strike' style='color:red'>red</span>.  Click the author or title to search for them again, and then add them to your e-shelf again, and then remove the outdated version.";
            $("table.folderName.EXLEShelfFolderContentsTable tr td:first").append(msg);     
        }

      }

})();


 /* @filename: js/payfines.js ---------------------------------------------- */ 
// Author: lesley.lowery@wwu.edu 
// Date: February 2016
// Purpose: this script adds a "Pay Fines" button to the fines list in Primo's My Account page.

 "use strict";
 
 WWU_PRIMO.payfines = (function() {
     
 var activity = WWU_PRIMO.common.getValue("activity");
     
 if ((activity == "fees") && ($('.MyAccount_FineAndFees_0').length)) {
     $(".EXLMyAccountMainHeader").append("<div title='Pay Fines now via CashNet' id='payfinesButtonDiv' class='payfinesButtonDiv'><span class='payfinesButton'><span class='EXLHiddenCue'>Press button to pay your fines at Cash Net</span><button id='payfinesButton' type='button' tabindex='0'>Pay Fines</button></span></div>");
// Add a Pay Fines button to the header line above the fines & fees table if there are fines present in the table.
 }
 
 $(".payfinesButton").on("click", function() {
    // when someone clicks on the 'Pay Fines' button, redirect to the CashNet URL
    window.location = ("https://commerce.cashnet.com/Wilson_Library")
    });
    
})();   