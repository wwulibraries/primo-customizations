
 WWU_PRIMO.didYouMean = function() {

    function showDidYouMeanOptions(search_query) {
        if (search_query) {
            // "were you looking for ___?"; thanks to http://smartsearch.uiowa.edu for the idea.  :)
            var search_query_lowercase = search_query.toLowerCase();
            var looking_for = "";

            var json_url = WWU_PRIMO.file_host  + "/were-you-looking-for.json?callback=?";
            // http://stackoverflow.com/questions/6849802/jquery-getjson-works-locally-but-not-cross-domain
            // http://shancarter.github.io/mr-data-converter/
           
            var getLookingForJson = $.ajax({
                'url': json_url,
                'dataType': "jsonp",
                cache : true,
                jsonpCallback: 'findMatch', // specify the callback name if you're hard-coding it
                success: function(data){
                    $.each(data, function(key, value){
                        var service_lowercase = value.service.toLowerCase();
                        var foundIt = service_lowercase.indexOf(search_query_lowercase);
                        if (foundIt !=-1) {
                            var looking_for = '<div id="lookingFor">Were you looking for <a target="_" href="' + value.URL + '">' + value.service + '</a>? </div>';
                            $("#resultsTileNoId").prepend(looking_for);
                        }
                    });                            

                }
            });
        }
    }
}();
