
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds stylesheets for different modes

 "use strict";
 
WWU_PRIMO.css = (function() {

    var mode = WWU_PRIMO.mode;
    var fn = WWU_PRIMO.fn;
    var lnk = null;

    if (mode == "Advanced" ) {
        lnk = document.createElement('link');
        lnk.type='text/css';
        lnk.href = WWU_PRIMO.file_host + '/wwu/css/advanced.css';
        lnk.rel='stylesheet';
        document.getElementsByTagName('body')[0].appendChild(lnk);            
    }

    var looking_for = new RegExp('^Browse');
    var mode_browse = looking_for.test(mode);
    var fn_browse = looking_for.test(fn);

    if  (mode_browse || fn_browse) {
        lnk = document.createElement('link');
        lnk.type='text/css';
        lnk.href = WWU_PRIMO.file_host + '/wwu/css/browse.css';
        lnk.rel='stylesheet';
        document.getElementsByTagName('body')[0].appendChild(lnk);       

    }

    if  ((fn == "almaAzSearch") || (fn == "goAlmaAz")) {
        lnk = document.createElement('link');
        lnk.type='text/css';
        lnk.href = WWU_PRIMO.file_host + '/wwu/css/az.css';
        lnk.rel='stylesheet';
        document.getElementsByTagName('body')[0].appendChild(lnk);            
    }

})();
