
// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose:: this script includes workarounds or features that are most likely only useful at wwu.edu - not recommended unless you need them.

 "use strict";
 
WWU_PRIMO.uncommon= (function() {

    // 21 July 2015 - we have a hidden 'default' tab to accommodate  links to that previously-used tab name (pre-tab-scopes/July 2015)
    // if the tab value equals 'default', then we should change it to 'everything' to start phasing it out for future permalinks and links
    var tab =  WWU_PRIMO.common.getValue("tab");      // sometimes the tab is not in the querystring;
    if (tab == "default") {
        $("#tab").val("everything");
        $("#exlidTab2").addClass("selected");
    }

})();
