// Author: lesley.lowery@wwu.edu 
// Date: February 2016
// Purpose: this script adds a "Pay Fines" button to the fines list in Primo's My Account page.

 "use strict";
 
 WWU_PRIMO.payfines = (function() {
	 
 var activity = WWU_PRIMO.common.getValue("activity");
	 
 if ((activity == "fees") && ($('.MyAccount_FineAndFees_0').length)) {
	 $(".EXLMyAccountMainHeader").append("<div title='Pay Fines now via CashNet' id='payfinesButtonDiv' class='payfinesButtonDiv'><span class='payfinesButton'><span class='EXLHiddenCue'>Press button to pay your fines at Cash Net</span><button id='payfinesButton' type='button' tabindex='0'>Pay Fines</button></span></div>");
// Add a Pay Fines button to the header line above the fines & fees table if there are fines present in the table.
 }
 
 $(".payfinesButton").on("click", function() {
    // when someone clicks on the 'Pay Fines' button, redirect to the CashNet URL
    window.location = ("https://commerce.cashnet.com/Wilson_Library")
    });
	
})();	