
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds  "jump to page" functionality next to the pagination links

 "use strict";
 
WWU_PRIMO.jump = (function() {

    var searchPage = document.location.href.toLowerCase().indexOf("search.do");
    if (searchPage != -1) {
        // add a 'go to page' near the pagination links
         var  current_count = $.trim($("#resultsNumbersTile h1 em:first").text());        // global variable
        var numResults = current_count.replace(',','');
            numResults = parseInt(numResults);

        var resultsPerPage = $('#exlidResultsTable').find("h2.EXLResultTitle").length;

        var numPagesTotal = Math.round(numResults / resultsPerPage);
        var gotoPageTooltip = numPagesTotal + " pages total";

        if (numPagesTotal > 1) {
            var currentPageNumber = $("span.EXLDisplayedCount.EXLBriefResultsPaginationPageCount:eq(1)").text();
            var gotoPage = '<div class="jump2page"> jump to page <input type="text" size="3" name="gotoPage" class="gotoPageInput" value="' + currentPageNumber + '"> </div> <div class="gotoPageInputToolTip">enter the page number you want to jump to, and then press tab<br><br>' + gotoPageTooltip + ' </div> <span class="spinner"> &nbsp; </span> <span class="pageLoadDelay"> Sorry for the delay; this slow page load has been reported to the administrator. </span> </div>  ';
            var gotoPageBottom= '<div class="jump2page"> jump to page <input type="text" size="3" name="gotoPage" class="gotoPageInput" value="' + currentPageNumber + '"> </div>   <div class="gotoPageInputToolTip Bottom">enter the page number you want to jump to, and then press tab<br><br>' + gotoPageTooltip + ' </div><span class="spinner"> &nbsp; </span> <span class="pageLoadDelay"> Sorry for the delay; this slow page load has been reported to the administrator. </span> </div>  ';

            $("#resultsNavNoId").prepend(gotoPage);
            $("#resultsNavNoIdBottom").prepend(gotoPageBottom);

        }

        $("a.EXLPrevious.EXLBriefResultsPaginationLinkPrevious.EXLBriefResultsPagination").hover(
          function() {
            $(".gotoPageInputToolTipLinks").show();
          }, function() {
            $(".gotoPageInputToolTipLinks").hide();
          }
        );
        $("a.EXLPrevious.EXLBriefResultsPaginationLinkPrevious.EXLBriefResultsPagination").blur(
          function() {
            $(".gotoPageInputToolTipLinks").hide();
          }
        );


        $(".gotoPageInput").hover(
          function() {
            $(".gotoPageInputToolTip").show();
          }, function() {
            $(".gotoPageInputToolTip").hide();
          }
        );
        $(".gotoPageInput").blur(
          function() {
            $(".gotoPageInputToolTip").hide();
          }
        );


       $(document).on('change', '.gotoPageInput', function() {

            var start = new Date;
            var seconds = 0;
            setInterval(function() {
               seconds = (new Date - start) / 1000;
                if (seconds > 5) {
                    $(".pageLoadDelay").show();
                    ga('send', 'event', 'slowPageLoad', 'gotoPageNumber', search_query);
                }

            }, 1000);

            $(".spinner").show();
         
            var gotoPageNumber = $(this).val();
            var gotoIndex = (10 * (gotoPageNumber - 2)) + 1;
                gotoIndex = Math.max(0, gotoIndex);     // convert negative values to zero
            var search = location.search;
            var newSearch = null;

            if (gotoIndex === 0) {
                newSearch = search.replace("?", "ct=&pag=&indx=" + gotoIndex + "&");
            } else {
                newSearch = search.replace("?", "?ct=Next+Page&pag=nxt&indx=" + gotoIndex + "&");
            }

            location.search = newSearch;
        });
    }

})();
