
// Author: david.bass @ w w u . e d u  
// Date: November 2015
// Purpose: this script replaces the standard header with a modified version (that is keyboard accessible - thanks to Kate Deibel  @ UW.edu and  Terrill Thompson for their help with this)

 "use strict";

WWU_PRIMO.header = (function() {

    var vid = WWU_PRIMO.vid;
    var mode = WWU_PRIMO.mode;

    var search_query = "";
    if (mode == "Basic") {
        search_query =  WWU_PRIMO.common.getValue("search_field");
    }

    if (mode == "Advanced") {
        search_query = $.trim($("#input_freeText0").val());
    }

    var menu_script = WWU_PRIMO.file_host + 'js/menubar.js';

    $.getScript( menu_script , function(data, textStatus){
        var menu1 = new menubar('kba_nav', false);
    });

    var UserTile = $("#exlidUserAreaTile").html();      // copy the contents of this into memory
    $("#newMenu").prependTo("body");        // move the newMenu to the top of the body

    var loginLogoutLink = "";
    var loginLogoutTitle = "";
    var targetURL = encodeURIComponent(document.location.href);

     var userName = $.trim($('.EXLUserNameDisplay').text());        // copy the user's name into memory;

    if (userName !== "Guest") {
        var hasComma = userName.toLowerCase().indexOf(",");      // a Guest will not have a comma
        if (hasComma != -1) {
            userName = userName.split(',');     // split the name at the comma
            userName = userName[1];             // use what follows that
            userName = userName.split(' ');     // split the name at the first space
            userName = userName[1];             // use what follows
        }

        userName = "Hello " + userName + " :)";
        loginLogoutLink = "/primo_library/libweb/action/logout.do?loginFn=signout&vid=" + vid + "&targetURL=" + targetURL;

        loginLogoutTitle = "Logout";
        $("#myAccountMenuHeading").html(userName);
        $("#loginLink").addClass("EXLSignOut");

    } else {
        userName = "";
        loginLogoutLink = "/primo_library/libweb/action/login.do?loginFn=signin&amp;initializeIndex%3dtrue&amp;vid=" + vid + "&amp;targetURL=" + targetURL;

        loginLogoutTitle = "Sign-in";
        $("#myAccountMenuHeading").html("<a id='loginLinkMenu' href='" + loginLogoutLink + "'>Sign-in</a>");
    }

    $("#loginLink").attr("href", loginLogoutLink);
    $("#loginLink").attr("title", loginLogoutTitle);
    $("#loginLink-tooltip").text(loginLogoutTitle);

    $("span#username").html(userName);


    if (search_query == "") {
        var worldcat_url = "http://ezproxy.library.wwu.edu/login?url=https://www.worldcat.org";
        $("#Worldcat-Menu-Link").attr("href", worldcat_url);
    } else {
       var worldcat_url = "http://ezproxy.library.wwu.edu/login?url=https://www.worldcat.org/search?qt=worldcat_org_all&q=" + search_query;
        $("#Worldcat-Menu-Link").attr("href", worldcat_url);
     }


    $("a.submenuitem.exlibris").each(function() {
        // append the vid to each ExLibris url in the new menu
        var thisHref = $(this).attr("href");
        var newHref = thisHref + "&vid=" + vid;
        $(this).attr("href", newHref);
    });

})();
