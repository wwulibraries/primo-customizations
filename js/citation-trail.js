
// Author: lesley.lowery @ wwu.edu 
// Date: May 2016
// Purpose: This script makes the citation trail buttons open in a new tab.

 "use strict";

$(".citeBtn a").attr('target','_blank');