
// Author: David . Bass @ w w u . e d u 
// Date: April / July 2016
// this requires  jQuery.PRIMO

 "use strict";

WWU_PRIMO.pinSelectedFacets = (function() {
    $(document).ready(function() {

        var vid = WWU_PRIMO.vid;
        var mode = WWU_PRIMO.mode;
        var institution = "WWU";
        var keep_facets_info, keep_facets_info2, widget_container = "";

        function convertQuerystringToHiddenFields (formId) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            var look_for = ['rfnId','ct','fctN','fctV','rfnGrp','mulIncFctN','fctIncV','mulExcFctN','rfnGrpCounter','rfnIncGrp','fctExcV','rfnExcGrp','facet-pinned'];

            // now go through each of the facet variables in the querystring and add them as hidden fields, to make them persist after the search;
            for (var i = 0; i < sURLVariables.length; i++)  {
                var sParameterName = sURLVariables[i].split('=');
                if (typeof sParameterName[0] == "string") {

                    if (look_for.indexOf(sParameterName[0]) != -1) { 
                        // this is one of the fieldnames in look_for;      
                        var thisFieldName = decodeURIComponent(sParameterName[0]);
                        var thisFieldValue = decodeURIComponent(sParameterName[1]);

                        var vlTest = /(vl\()/;
                        var vlField = vlTest.test(thisFieldName);
                        if (!vlField) {
                            var theForm = document.getElementById(formId);
                            var thisField  = document.createElement("input");
                            thisField.setAttribute("type","hidden");
                            thisField.setAttribute("name", thisFieldName);
                            thisField.setAttribute("value", thisFieldValue);
                            thisField.setAttribute("class", "facetpin-hidden");
                            theForm.appendChild(thisField);        
                        }
                    }
                }
            }
        }

        var searchPage = document.location.href.toLowerCase().indexOf("search.do");
        if (searchPage != -1) {
            // any facets applied?
            // if so, let's add a pin icon to allow the user to make those persist after a new search (otherwise they disppear after each new/different search)

            var keep_facets_button = ' <span id="keep_facets_container"> <button id="facetpin" title="check this box to make the applied filters persist" class="facetpin"> Keep filters </button> <i id="more_info_icon" class="more_info_icon fa fa-info-circle"  title="What is this?"></i>';
            var keep_facets_button2 = ' <span id="keep_facets_container2"> <button id="facetpin2" title="check this box to make the applied filters persist" class="facetpin"> Keep filters </button> <i id="more_info_icon2" class="more_info_icon fa fa-info-circle"  title="What is this?"></i>';

            widget_container = '<div id="keep_facet_code_container"><i class="closeWidget fa fa-times" aria-hidden="true"></i>  &quot;Keep filters&quot; lets you modify your search without having to re-apply the filters.  This feature can also provide you with a customized search box that you can embed on your own website, with these filters pre-applied.  ';

            var isLoggedIn = jQuery.PRIMO.session.user.isLoggedIn();

            if (isLoggedIn) {
                var user_email = jQuery.PRIMO.session.user.email;            

                widget_container += ' <br><br> To request a customized search widget, click the button below, and we will send the code to ' + user_email + '.  \
                        <div> \
                            <form> \
                                <div id="request-widget-container"> \
                                    <div> \
                                         <input type="hidden" id="your-email-address" name="your-email-address" value="' + user_email + '" size="30" /> \
                                    </div> \
                                    <input type="hidden" name="widget-code" id="widget-code" value=""> \
                                    <input type="hidden" name="note" id="note" value=""> \
                                    <div> \
                                        <input  id="submit-widget-form" class="tab-button" type="submit" name="submit-widget-form" value="Send me the widget code" /> \
                                    </div> \
                                </div> \
                                </form> \
                                <div id="widget-preview"> \
                                    <br> Here is a screenshot of what the widget looks like: \
                                    <img src="' + WWU_PRIMO.file_host + 'images/widget.png" alt="screenshot of widget"> \
                                </div> \
                            </div> \
                        </div> </span> \
                    ';        

            } else {
                 widget_container += '  To request a customized search widget, please sign-in and open this dialog again. ';
            }

            keep_facets_info = keep_facets_button + widget_container;
            keep_facets_info2 = keep_facets_button2 + widget_container;

             // if any filters/facets have been applied, add the "keep filters" checkboxes
             var filtersExist = $("div").hasClass("EXLRefinementRibbonWithExclude");
             if (filtersExist) {
                $(".EXLRefinementRibbonWithExclude").append(keep_facets_info);  // right side
                $(".filterButtonDiv").append(keep_facets_info2);                                    // left side
             }

            var querystring_pinned = document.location.href.toLowerCase().indexOf("&facet-pinned=true");
            if (querystring_pinned != -1) {
                    convertQuerystringToHiddenFields("searchForm");
                    $(".facetpin").toggleClass("pinned");                              // change the state (to pinned, or un pinned)
                    $(".facetpin").attr("title", "filters will be applied during next search");
            }            

            $(".closeWidget").on("click", function() {
                // hide the widget code;
                $("#keep_facet_code_container").hide();
            });


            var widgetCode = '<link href="' + WWU_PRIMO.file_host + 'wwu/css/search-box-widget.css" rel="stylesheet" type="text/css"> ' +
                ' <div id="search-box-standalone"> ' +
                '   <img alt="OneSearch logo" id="onesearchLogo" src="' + WWU_PRIMO.file_host + 'images/onesearch-custom.png"> ' +
                '  <div id="exlidSearchRibbon"> ' +
                '      <form class="hideSubmitButton-processed" id="submit-to-primo"> ' +
                '          <label class="hidden" for="search_field">Search For:</label> <input accesskey="s" id="search_field" name="vl(freeText0)" placeholder="Use &quot;quotes&quot; for exact match." value="" type="text"> ' + 
                '          <button class="button box-shadow-outset" id="goButton" title="basic search" type="submit" value=""></button>    ';

            // add the hidden fields to the widget
            var hidden_facets = "";
            $(".facetpin-hidden").each(function() {
                hidden_facets += $(this).prop('outerHTML');
            });

            widgetCode += hidden_facets;

            var current_scope = "";
            if (mode == "Advanced") {
                current_scope = $.trim($("#exlidSearchTabs li.selected span").attr("id"));
                current_scope = current_scope.replace("defaultScope","");
            } else {
                current_scope = $(".EXLSelectedScopeId").val();
            }

            var tab = WWU_PRIMO.common.getValue("tab");      // sometimes the tab is not in the querystring;

            widgetCode += ' '  +
                '        </form>  </div>  <div id="search_tip_container"> Search tip: <div id="search_tip"></div></div> </div> ' +
                '  <scr'+'ipt> ' +
                '       var wwu_primo_search_widget =  (function(){  ' +
                '          return {  ' +
                '             version : 1 ' +
                '            , cycle: true ' +
                '           , delay: 5000 ' +
                '           , submit_to : "//' + window.location.hostname + '/primo_library/libweb/action/dlSearch.do?vid=' + vid + '&mode=' + mode + '&search_scope=' + current_scope + '&tab=' + tab + '&institution=' + WWU_PRIMO.institution + '&facet-pinned=true"  ' +
                '           , placeholders: ["use \\"quotes\\" for an exact match, like this:  \\"Teaching Children Mathematics\\"", "use upper-case AND to refine your search - example: glucose AND \\"dextrose monohydrate\\"", "use ? for single character wildcard search (e.g. \'wom?n\')",   ' +
                '           "use * for multiple character wildcard search (e.g. \'cultur*\')", "use upper-case NOT to exclude a term, like this: gluten NOT celiac", "use OR and parentheses like this: (cycling NOT safety) OR helmet"]  ' +
                '        }  ' +
                '       })();  ' +
                '  </scr'+'ipt> ' +
                '  <scr'+'ipt src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></scr'+'ipt> ' +
                ' <scr'+'ipt src="' + WWU_PRIMO.file_host + 'js/search-box-widget.js"></scr'+'ipt> ' +
                '     ';



            $("#more_info_icon").on("click", function() {
                // show the widget code (on the right);
                $("#note").val("right");
                $("#keep_facet_code_container").removeClass("left").toggle();
                $("#widget-code").val(widgetCode);
            });

            $("#more_info_icon2").on("click", function() {
                // show the widget popup (on the left);
                $("#note").val("left");
                $("#keep_facet_code_container").addClass("left").toggle();
                $("#widget-code").val(widgetCode);
            });


        /* record each time the "Keep filters" button was clicked, and what the query was as a Google Analytics event */
        $(document).on('click', '#facetpin', function() {
            ga('send', 'event', 'button', 'facetpin');
        });

        $(document).on('click', '#facetpin2', function() {
            ga('send', 'event', 'button', 'facetpin2');
        });


        $(document).on('click', '#submit-widget-form', function(evt) {
            evt.preventDefault();
            submitWidgetForm();
        });

        function submitWidgetForm() {
            $('html, body, div, button').css("cursor", "wait");
            // var submit_to_url = "http://test.library.wwu.edu/rest/submission";
            var submit_to_url = "https://library.wwu.edu/rest/submission";
            var email_address =  $("#your-email-address").val();
            var widget_code =  $("#widget-code").val();
            var note = "right";

            /* we cannot use $.ajax and mode = post because the Primo code changes it to a GET request, which causes this to fail, but forcing post via $.post works */
            var post = $.post(submit_to_url, { "webform": "67020d88-2670-4ac0-9966-954e7ab5f857", "submission": { "data" : { "1":{"values":{"0": $("#your-email-address").val() }}, "2":{"values":{"0": $("#widget-code").val() }}, "3":{"values":{"0": $("#note").val() }} }}})
              .always(function(data) {
                    $('html, body, div, button').css("cursor", "default");

                    if (data.statusText == "OK") {
                        $("#keep_facet_code_container").html("Thank you!").fadeOut(3000);
                    } else {
                        $("#keep_facet_code_container").html("An error occurred.").fadeIn(100);                    
                    }
            });
        }

        $(".facetpin").on("click", function() {
                var wasPinned = $(this).hasClass("pinned");      // determine the current state of the pin
                $(".facetpin").toggleClass("pinned");                              // change the state (to pinned, or un pinned)
                if (wasPinned) {                                                             // based on the original state, lets either remove or add the hidden fields to make the facet persist
                    // remove the hidden fields, because the user un-pinned it;
                    // $(".more_info_icon").hide();
                    $("input.facetpin-hidden").remove();
                    $(".facetpin").attr("title", "check this box to make the applied filters persist");            // change the title back to the original

                    var current_url = document.location.href;
                    var new_url = current_url.replace("&facet-pinned=true", "");      // remove &facet-pinned from the querystring
                    history.replaceState({}, '', new_url);

                    // remove the facet-pinned=true from all of the other links in Primo
                    $("a").each(function() {
                        var this_url = $(this).attr("href");
                        if (this_url) {
                            var has_pinned = this_url.indexOf("&facet-pinned=true");
                            if (has_pinned != -1) {
                                var new_url = this_url.replace("&facet-pinned=true","");
                                $(this).attr("href", new_url);                                                    
                            }
                        }
                    })

                } else {
                    // if was not pinned, but now it is
                    // add the hidden fields;                
                    // $(".more_info_icon").show();
                    $(".facetpin").attr("title", "filters will be applied during next search");
                    var new_url = location.href + (location.search ? "&" : "?") + "facet-pinned=true";
                    history.replaceState({}, '', new_url);
                    convertQuerystringToHiddenFields("searchForm");

                    // if 'keep filters' is checked, and the user switches to a different tab, they will lose their filters.  warn them.
                    var current_tab_id = $(".EXLSearchTabSelected").attr("id");
                    $(".EXLSearchTab").on("click", function() {
                        // if the user tries to change the tab, warn them that they will lose their filters (because each tab/scope has different filters);
                        var tabIsSelected = $(this).hasClass("EXLSearchTabSelected");
                        if (!tabIsSelected) {
                            return  leaveCurrentTab();
                        };

                    });
                }
           });
        }

        function leaveCurrentTab() {
              var retVal = confirm("You will lose your selected filters if you change to a different tab.  Click 'cancel' to stay on the current tab.");
               if( retVal == true ){
                  return true;
               }
               else{
                  return false;
               }
           }
        });

})();