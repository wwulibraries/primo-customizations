
// Original author: david . bass @ w w u . e d u  
// enhancements by Tamara . Marnell  @ p c c . e d u 
// Purpose: this script creates a short URL (permalink)  of an otherwise long query in Primo - useful for sending via email

 "use strict";

WWU_PRIMO.permalink = (function() {

    var vid = WWU_PRIMO.vid;
    var mode = WWU_PRIMO.mode;
    var permalink_key =  WWU_PRIMO.common.GetURLParameter("permalink_key");
    var permaLinkPretty = null;
    var expand_results_for = WWU_PRIMO.expand_results_for;

  if (mode == "Advanced") {
        if (permalink_key) {
            // if this is an advanced search, submit the page after it loads because  it is not possible (yet) to store all of the query parameters in the querystring, so we have to re-submit to make it work exactly as it was originally;
            var resultsDiv = document.getElementById("resultsTileNoId");
            resultsDiv.innerHTML = "<i class='fa fa-cog fa-spin fa-2x'></i> Please wait...";
            document.getElementById("exlidFacetTile").innerHTML = "";
            // append the facets to the form inputs so they will be included in the re-submission
            convertQuerystringToHiddenFields("searchForm");
            var theForm = document.getElementById("searchForm");
            theForm.submit();
        }
    }


    function convertQuerystringToHiddenFields (formId) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)  {
            var sParameterName = sURLVariables[i].split('=');
            if ((typeof sParameterName[0] == "string") && (sParameterName[0]  != "permalink_key")) {        // do not add the permalink_key to prevent a loop
                    var thisFieldName = decodeURIComponent(sParameterName[0]);
                    var thisFieldValue = decodeURIComponent(sParameterName[1]);

                    var vlTest = /(vl\()/;
                    var vlField = vlTest.test(thisFieldName);
                    if (!vlField) {
                        var theForm = document.getElementById(formId);
                        var thisField  = document.createElement("input");
                        thisField.setAttribute("type","hidden");
                        thisField.setAttribute("name", thisFieldName);
                        thisField.setAttribute("value", thisFieldValue);
                        theForm.appendChild(thisField);        
                    }
            }
        }
    }



    // TODO: instead of removing the permalink_key for advanced searches, set some other flag that will prevent the looping problem caused by the advanced search re-submit workaround process.

    function buildDlSearchParameters() {
        // translate the current search.do parameters into dlSearch.do parameters
        // https://developers.exlibrisgroup.com/primo/apis/deeplinks/brief

        var institution = WWU_PRIMO.institution;
        var srt = WWU_PRIMO.common.getValues("srt");
        var submit = WWU_PRIMO.common.GetURLParameter('Submit');
        var tab = WWU_PRIMO.common.getValue("tab");      // sometimes the tab is not in the querystring;


        // facets
        var ct = WWU_PRIMO.common.getValues('ct');
        var fctN = WWU_PRIMO.common.getValues('fctN');
        var fctV = WWU_PRIMO.common.getValues('fctV');
        var rfnGrp = WWU_PRIMO.common.getValues('rfnGrp');
        var rfnGrpCounter = WWU_PRIMO.common.getValues('rfnGrpCounter');
        var mulIncFctN = WWU_PRIMO.common.getValues('mulIncFctN');
        var fctIncV = WWU_PRIMO.common.getValues('fctIncV');
        var rfnIncGrp = WWU_PRIMO.common.getValues('rfnIncGrp');
        var mulExcFctN = WWU_PRIMO.common.getValues('mulExcFctN');
        var fctExcV = WWU_PRIMO.common.getValues('fctExcV');
        var rfnExcGrp = WWU_PRIMO.common.getValues('rfnExcGrp');
        var rfnId = WWU_PRIMO.common.getValues('rfnId'); 

        var facets = rfnId + ct + fctN + fctV + rfnGrp + mulIncFctN + fctIncV + mulExcFctN + rfnGrpCounter + rfnIncGrp + fctExcV + rfnExcGrp;

        var querystring_pinned = document.location.href.toLowerCase().indexOf("&facet-pinned=true");
        // this relates to pin-selected-facets.js
        if (querystring_pinned != -1) {
            facets += "&facet-pinned=true";
        }

        var query = "";
        var dlQuery0 = "";
        var dlQuery1 = "";
        var dlQuery2 = "";
        var advSearchParameters = "";
        var query_nocomma = "";

        function getAdvQuery(i) {
            var position = i;
            var position_id = i + 1;
            var scope_id = "#exlidInput_scope_" + position_id;
            var scope_name = $(scope_id).attr("name");
            var scopeValue = $(scope_id + " option:selected").val();        
            var scope = "&" + scope_name + "=" + scopeValue;        
            var precisionOperator_id = "#exlidInput_precisionOperator_" + position_id;
            var precisionOperator_name = $(precisionOperator_id).attr("name");
            var precisionOperator_value = $(precisionOperator_id + " option:selected").val();
            var precisionOperator = "&" + precisionOperator_name + "=" + precisionOperator_value;
            var boolOperator_id = "#exlidInput_boolOperator_" + position_id;
            var boolOperator_name = $(boolOperator_id).attr("name");
            var boolOperator_value = $(boolOperator_id + " option:selected").val();
            var boolOperator = "&" + boolOperator_name + "=" + boolOperator_value;
            var query_id = "#input_freeText" + position;
            var query = encodeURIComponent($.trim($(query_id).val()));
            query_nocomma = query.replace(/,/g, '+');       // replace comma with plus
            query_nocomma = query_nocomma.replace(/"/g, '%22');      // replace double quotes with %22
            query_nocomma = query_nocomma.replace(/'/g, '%27');      // replace single quotes with %27

            if (query_nocomma) {
                var value  = scope + precisionOperator + "&vl(freeText" + position + ")=" + query_nocomma + boolOperator;
                return value;                    
            } else {
                return "";
            }
        }


        if (mode === "Advanced") { 
            // advanced fields        

            // 1st query
            dlQuery0 = getAdvQuery(0);

            // 2nd query
            dlQuery1 = getAdvQuery(1);

            //  3rd query
            dlQuery2= getAdvQuery(2);

            //var operator = GetURLParameter("&vl%28boolOperator2%29");
            var pubDateName = $("#exlidInput_publicationDate_").attr("name");
            var pubDate = "&" + pubDateName + "=" + $("#exlidInput_publicationDate_ option:selected").val();        // publication date

            var mediaTypeName = $("#exlidInput_mediaType_").attr("name");
            var mediaType = "&" + mediaTypeName + "=" + $("#exlidInput_mediaType_ option:selected").val();              //  media Type

            var languageName = $("#exlidInput_language_").attr("name");
            var language = "&" + languageName + "=" + $("#exlidInput_language_ option:selected").val();               // language

            var startDayName = $("#exlidInput_drStartDay_").attr("name");
            var startDayValue = $("#exlidInput_drStartDay_ option:selected").val();
            var startDay = "&" + startDayName + "=" + startDayValue;             // start day

            var startMonthName = $("#exlidInput_drStartMonth_").attr("name");
            var startMonthValue = $("#exlidInput_drStartMonth_ option:selected").val();
            var startMonth = "&" + startMonthName + "=" + startMonthValue;           // start month

            var startYearName = $("#exlidInput_drStartMonth_ ~ input").attr("name");
            var startYearId = "#" + $("#exlidInput_drStartMonth_ ~ input").attr("id");
            var startYearValue = $(startYearId).val(); 
            var startYear = "&" + startYearName + "=" + startYearValue;            // start year
      
            var endDayName = $("#exlidInput_drEndDay_").attr("name");
            var endDayValue = $("#exlidInput_drEndDay_ option:selected").val();
            var endDay = "&" + endDayName + "=" + endDayValue;             // end day

            var endMonthName = $("#exlidInput_drEndMonth_").attr("name");
            var endMonthValue = $("#exlidInput_drEndMonth_ option:selected").val();
            var endMonth = "&" + endMonthName + "=" + endMonthValue;           // end month

            var endYearName = $("#exlidInput_drEndMonth_ ~ input").attr("name");
            var endYearId = "#" + $("#exlidInput_drEndMonth_ ~ input").attr("id");
            var endYearValue = $(endYearId).val();
            var endYear = "&" + endYearName + "=" + endYearValue;             // end year

            advSearchParameters =  pubDate + mediaType + language + startDay + startMonth + startYear + endDay + endMonth + endYear ;
        } else {
            // basic search;
            var dlScope = $(".EXLSelectedScopeId").val();
            query = encodeURIComponent($("#search_field").val());
            query_nocomma = query.replace(/,/g, '+');       // replace comma with plus
            query_nocomma = query_nocomma.replace(/"/g, '%22');      // replace double quotes with %22
            query_nocomma = query_nocomma.replace(/'/g, '%27');      // replace single quotes with %27
            dlQuery0 = "&query=any,contains," + query_nocomma + '&search_scope=' + dlScope;
        }

        var expand_results_querystring = "";
        if (tab === expand_results_for) {
            expand_results_querystring = "&pcAvailabiltyMode=true";
        }

       var dlPath =  'vid=' + vid + '&mode=' + mode + '&tab=' + tab + '&institution=' + institution +  dlQuery0 + dlQuery1 + dlQuery2 + facets + advSearchParameters + srt + expand_results_querystring;

        return dlPath;
    }

     // Print permalink div
      function printPermalink(url) {
        return "<strong>Permalink:</strong> <input id='permalink-value' type='text' value='" + url + "' onclick='$(this).select();'>";
      }


    var displayPage = document.location.href.toLowerCase().indexOf("display.do");
    if (displayPage != -1) {
        (function(){
            // only run this code on the display page for now; do not run in search results page
            // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
           var  exli_permaLink_orig = $(".EXLButtonSendToPermalink").find("a").attr("href");
           var exli_permalink = WWU_PRIMO.common.recreateExLiPermalink(exli_permaLink_orig);
           var exli_permaLink_final = window.location.protocol + "//" + window.location.hostname + "/" + exli_permalink;
           var permaLink = "<span class='wwu_permalink'>" + exli_permaLink_final + "</span>";
            permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div>";
            $("#resultsListNoId").prepend(permaLinkPretty);
        }) ();
    }


    function savePermaLink(permaLink) {
        $('html, body, div, button').css("cursor", "wait");
        // var create_permalink_url = WWU_PRIMO.file_host + "permalinks/create.php";
        var create_permalink_url = WWU_PRIMO.CREATE_PERMALINK_URL;
        $.ajax({
            url: create_permalink_url,
            crossDomain: true,
            data: {query: permaLink},
            dataType: 'html',
            success: function(responseData, textStatus, jqXHR) {
                $('html, body, div, button').css("cursor", "auto");
                var permaLinkId = responseData;
                var newPermalink = WWU_PRIMO.GOTO_PERMALINK_URL + permaLinkId;      
                $("#wwu-permalink").html(printPermalink(newPermalink)).fadeIn(100);
            },
            error: function (responseData, textStatus, errorThrown) {
                $("#wwu-permalink").html("Could not generate permaLink.  Please email " + WWU_PRIMO.error_email + " with information on how to reproduce this problem.").fadeIn(100);
                /* TODO: notify admin */
            }
        });
    }


    // Show button on search page
      if (WWU_PRIMO.search_query != '' && RegExp(/search\.do/i).test(window.location.href) && !RegExp(/query=facet_atoz/i).test(window.location.href) && !RegExp(/fn=Browse/i).test(window.location.href) ) {
        
            var permalink_div = "<div id='wwu-permalink'>";
            
            // If these are results from a permalink, display existing key
            if (permalink_key) {
              permalink_div += printPermalink(WWU_PRIMO.GOTO_PERMALINK_URL + permalink_key);    
            } else {        
            // Otherwise display button to generate permalink
              permalink_div += "<button id='generate-permalink'>Generate a permalink for these results</button>";
            }
            permalink_div += '</div>';
            
            // Add the permaLink container to the top of the results container
            $("#resultsHeaderNoId").prepend(permalink_div);
            
            // Add click handler to generate search button
            $(document).ready(function() {
              $('#generate-permalink').click(function(e) {
                e.preventDefault();
                var advPermaLink = buildDlSearchParameters();
                savePermaLink(advPermaLink);
                return false;
              });
            });
    }


    $(window).load(function() {
            $('a').each(function() {
                // remove the permalink_key variable from any links, so that they won't show the permalink except for the first time
                var _href = $(this).attr('href');
                if (_href) {
                    var new_href = _href.replace("permalink_key=", "original_plink_key=");
                    $(this).attr('href', new_href );   
                }
            });
    });


})();
