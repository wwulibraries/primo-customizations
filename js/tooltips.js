
// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose: this script  shows tooltips to help users understand the interface

 "use strict";
 
WWU_PRIMO.tooltips = (function() {

    $("#featureTour").click(function()  {
        setTimeout(showTooltips, 500);
    });

    function showTooltip (id) {
        var theId = "#" + id;
        $(theId).fadeIn(0).fadeOut(1800);       // show each tooltip if the search_query is empty
    }

    function resetTooltip (id) {
        var theId = "#" + id;
        $(theId).attr("style", "");       // show each tooltip if the search_query is empty
    }

    var timeouts = [];        // create an array to store each setTimeOut, so that we can cancel them later if we detect mouse/keyboard movement

    function showTooltips () {
        // on a new search, loop through the tooltips to show what the icons do
        $('.showme').each(function(i) {
            var id = $(this).attr("id");

            var delay = 2000 * i;
            var delayCheckbox = 1000 * i;

            if (id == "facetTour") {
                var theCheckboxId = "#" + $(this).parent().find(".facetcheckbox").attr("id");

                timeouts[i] = setTimeout(function() {
                    console.log(theCheckboxId);
                    $(theCheckboxId).click();
                }, delayCheckbox);

                timeouts[i] = setTimeout(function() {
                    console.log(theCheckboxId);
                    $(theCheckboxId).click();
                }, delayCheckbox);

                timeouts[i] = setTimeout(function() {
                    console.log(theCheckboxId);
                    $(theCheckboxId).click();
                }, delayCheckbox);

            }


            timeouts[i] = setTimeout(showTooltip, delay, id);
            var delay2 = 2000 + delay;  // give the tooltip time to run before removing the style that prevents it from being shown again
            setTimeout(resetTooltip, delay2, id);   // this removes the jQuery style="display:none" which prevents the tooltip from working
                // TODO: can we use a promise instead of this delay2?
        });
    }

    function setProp(id, prop, value) {
        console.log(id + " " + prop + " " + value);
        var id = "#" + id;
        $(id).prop(prop, value);
    }

    /* if we detect any mouse or keyboard movement, stop the automated tooltips */
    var pause = false;
    $(document).on('mousemove keypress', function(){
        for (var i = 0; i < timeouts.length; i++) {
            clearTimeout(timeouts[i]);
        }
    });


})();