
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script contains the functions needed by some or all of the components of our Primo enhancements

 "use strict";

WWU_PRIMO.common = (function() {
    
    var get_params = function(search_string) {
      var parse = function(params, pairs) {
        var pair = pairs[0];
        var parts = pair.split('=');
        var key = decodeURIComponent(parts[0]);
        var value = decodeURIComponent(parts.slice(1).join('='));

        // Handle multiple parameters of the same name
        if (typeof params[key] === "undefined") {
          params[key] = value;
        } else {
           params[key] = [].concat(params[key], value);
        }

        return pairs.length == 1 ? params : parse(params, pairs.slice(1));
      };

      // Get rid of leading ?
      return search_string.length == 0 ? {} : parse({}, search_string.substr(1).split('&'));
    };

    var params = get_params(location.search);

    return {
        "getValue" : function(fieldName) {
                var element = document.getElementById(fieldName);
                var element_value = null;
                if (element !== null) {
                    element_value = document.getElementById(fieldName).value;
                }
              var value = this.GetURLParameter(fieldName) || element_value || "";
              return value;                                    
        } ,

        "GetParameter" : function (string, sParam) {
            // thanks to http://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript/11582513#11582513
            return decodeURIComponent((new RegExp('[?|&]' + sParam + '=' + '([^&;]+?)(&|#|;|$)').exec(string)||[,""])[1].replace(/\+/g, '%20'))||'';
        } ,

        "GetURLParameter" : function (sParam) {
            // thanks to http://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript/11582513#11582513
            return decodeURIComponent((new RegExp('[?|&]' + sParam + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||'';
        } ,

        "getValues" : function (fieldName) {
            //var isPresent = location.search.indexOf("&" + fieldName + "=");
            var lookingFor = new RegExp("[\?|&]" + fieldName + "=","i");
            var isPresent = location.search.search(lookingFor);
            if (isPresent != -1) {
                var thisValue = params[fieldName];
                var returnValue = "";
                if (typeof thisValue != "string") {
                    $.each(thisValue, function(index, value) {
                        // returnValue += "&" + fieldName + "=" + value;
                        returnValue += "&" + fieldName + "=" + value.replace(/&/g, "[amp]");
                        // console.log("returnValue = " + returnValue);
                    });                
                } else {
                    returnValue += "&" + fieldName + "=" + thisValue;
                }
                return returnValue;
            } else {
                return "";
            }
        } ,

        "recreateExLiPermalink" : function (exli_permaLink) {
            // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
            // var exli_permaLink = $(".EXLButtonSendToPermalink").find("a").attr("href");
            // this should return something like
                //    "permalink.do?docId=TN_springer_jour10.2165/00003495-200161130-00006&vid=WWU&fn=permalink"

            var docId = this.GetParameter(exli_permaLink,"docId");      // extract the docId value from that string
            var vid = this.GetParameter(exli_permaLink,"vid");           // extract the vid value from the exli_permalink string
            var exli_permaLink_final = vid + ":" + docId;     // put the pieces together
            return exli_permaLink_final;
        }


    };

})();

WWU_PRIMO.fn = WWU_PRIMO.common.getValue("fn");
WWU_PRIMO.mode = WWU_PRIMO.common.getValue("mode");
WWU_PRIMO.vid = WWU_PRIMO.common.getValue("vid");

WWU_PRIMO.search_query = null;

if (WWU_PRIMO.mode == "Basic") {
    WWU_PRIMO.search_query =  WWU_PRIMO.common.getValue("search_field");
}

if (WWU_PRIMO.mode == "Advanced") {
    WWU_PRIMO.search_query = $.trim($("#input_freeText0").val());
}
