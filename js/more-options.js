
// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds a "Didn't find what you were looking for?" link at the bottom of the search results 
// TODO: store visited URLs in localStorage, and when this button is clicked, ask the user if we can send the details of their session to the librarian? (similar to a crash report)
// NOTE: this currently only works in basic search (not advanced);

 "use strict";
 
WWU_PRIMO.moreOptions = (function() {

     if (WWU_PRIMO.mode == "Basic") {
        var search_query = WWU_PRIMO.common.getValue("search_field");

        var now = new Date().getTime();
        var now_plus = now + "a";

        var illiad_url = "https://www.illiad.library.wwu.edu/illiad.dll?Action=10&Form=10";
        $("#illiad_link").attr("href", illiad_url);
        $("#illiad_link").attr("target", now);

        var worldcat_url = "http://ezproxy.library.wwu.edu/login?url=https://www.worldcat.org/search?qt=worldcat_org_all&q=" + search_query;
        $("#repeat-in-worldcat-link").attr("href", worldcat_url);

        var didnt_find_link = ' <div id="didnt_find_div"> <button id="didnt_find_link" type="button" class="btn btn-link" data-toggle="modal" data-target="#didntFindModal">Additional Search + Request Options</button> </div> ';
        $("#resultsTileNoId").append(didnt_find_link);

        if ($("#pcAvailabiltyMode").length == 0) {
            // hide the extra-time option
            $("#include-results-extra-time").hide();
        } else {

            /*
            $(document).on("change",  "#extra-time", function() {
                var isChecked = $(this).prop("checked");
                if (isChecked) {
                    var newUrl = location.href + "&pcAvailabiltyMode=true";
                    window.location.href = newUrl;
                }
            });                
            */

        }

     }

})();
