
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script records events and sends them to Google Analytics
// NOTICE: configure the Google Analytics code and domain at the bottom of this file

 "use strict";
 
WWU_PRIMO.analytics = (function() {

    // initialize the variables used in this module;
    var a_class,  a_href,  a_text, a_action, a_id, resultsPerPage, hidden_field_id, boomValue, typeOfResult, journalTitle, src, a_title, a_title_parts, link_number;

    /* record each time the "Didn't find" button was clicked, and what the query was as a Google Analytics event */
    $('#didnt_find_link').on('click', function() {
      ga('send', 'event', 'button', 'Didnt_find_button', search_query);
    });


    /* add a counter to each results link, so that we can track which of the links was clicked (1st on page, 7th on page, etc...) */
    resultsPerPage = $('#exlidResultsTable').find("h2.EXLResultTitle").length;
    $('#exlidResultsTable').find("h2.EXLResultTitle").find("a").each(function(index) {
        var counter = index + 1;
        var resultOrderValue = "result " + counter + " of " + resultsPerPage;
        $(this).data("resultOrder", resultOrderValue);
        $(this).addClass("searchResultLink");
    });

    var openurlPage = document.location.href.toLowerCase().indexOf("/primo_library/libweb/action/openurl");
    if (openurlPage != -1) {
        // record openurl page hits (assuming the "View It" tab is selected) as a GA event (like the search results > View It links are recorded) 
        src = GetURLParameter('sid');
        hidden_field_id = "#getit1_0";
        boomValue = $(hidden_field_id).val();
        journalTitle = decodeURIComponent(WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle'));
        ga('send', 'event', 'openurl', 'journal: ' + journalTitle, 'source: ' + src);      // record the order of the link (for search results)
    }


    $("#search_field").on("blur", function() {
        var search_query = $("#search_field").val();
        if (typeof search_query !== 'undefined') {
            ga('send', 'event', 'search_query', 'blur', search_query);      // record the search_query
        }
    });

    /* track each time a link is clicked as a Google Analytics event */
    $('a').on('click', function() {
        a_class = $(this).attr('class');
        a_href = $(this).attr('href');
        a_text = $.trim($(this).text());
        a_action = "href-click";
        a_id = $(this).attr("id");

        if (a_text == "View It") {
            // this is a "View It" link on the search results page; lets append the journal title to the event title
            a_action = "View It";
            src = $("#search_field").val();

            // which link is this?
            a_title = $(this).attr('title');
            a_title_parts = a_title.split('-');    
            link_number = $.trim(a_title_parts[1]);

            typeOfResult = 'journal';

            var displayPage = document.location.href.toLowerCase().indexOf("display.do");
            if (displayPage != -1) {
                hidden_field_id = "#getit2_0";
                boomValue = $(hidden_field_id).val();
                journalTitle = WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle');
                    journalTitle = decodeURIComponent(journalTitle);                    
            } else {
                hidden_field_id = "#getitonline1_" + link_number;
                boomValue = $(hidden_field_id).val();
                typeOfResult = WWU_PRIMO.common.GetParameter(boomValue, 'O');

                if (typeOfResult.indexOf("newspaper_article") > -1) {
                    typeOfResult = "newspaper_article";
                    journalTitle = WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle');
                    journalTitle = decodeURIComponent(journalTitle);
                } else if (typeOfResult.indexOf("article") > -1) {
                    typeOfResult = "article";
                    journalTitle = WWU_PRIMO.common.GetParameter(boomValue, 'rft.jtitle');
                    journalTitle = decodeURIComponent(journalTitle);
                } else if (typeOfResult.indexOf("ebook") > -1) {
                    typeOfResult = "ebook";
                    journalTitle = $(this).closest(".EXLSummary").find(".EXLResultTitle a").text();
                } else {
                    journalTitle = undefined;
                }

                if (journalTitle == "undefined") {
                    // ExLibris uses two different techniques to store the Journal Title, so this is the 2nd technique.
                    journalTitle = WWU_PRIMO.common.GetParameter(boomValue, "O8");       // please note, this is NOT zero eight; but capital oh eight.  grrr.
                        journalTitle = decodeURIComponent(journalTitle);    
                }
            }

            var eventAction = typeOfResult + ': ' + journalTitle;
            var eventLabel = 'query: ' + src;

            ga('send', 'event', 'View It', eventAction, eventLabel);      // record the order of the link (for search results)

        } else if (typeof a_class === 'undefined') {
            // this link doesn't yet have a class
            ga('send', 'event', 'link', a_action, a_text);      // record the text of the link
        } else {
            if (a_class.indexOf("searchResultLink") >= 0) {
                // this is a search result link - lets record the order, but not the title of the item
                a_action = "searchResult-order";
                a_order = $(this).data('resultOrder');
                ga('send', 'event', 'link', a_action, a_order);      // record the order of the link (for search results)
            } else {
                // this is some other type of link (probably a facet)

                if (a_class.indexOf("EXLBriefResultsPagination") >= 0) {
                    a_action = "page-number";     /// the user clicked on a "remove facet/filter" link

                    var pageNumber = parseInt(a_text);
                    if (pageNumber > 0) {
                        a_text = "goto page " + a_text;     /// the user clicked on one of the pagination numbers
                    } else {
                        a_text = "next page";     /// the user clicked on the "next page" icon
                    }
                }

                if (a_class.indexOf("EXLFirstRefinementElement") >= 0) {
                    a_action = "remove-filter";     /// the user clicked on a "remove facet/filter" link
                }

                if (a_class.indexOf("facetLink") >= 0) {
                    a_action = "apply-filter";     // the user clicked on a facet (to apply a filter)
                }

                ga('send', 'event', 'link', a_action, a_text);      // record the text of the link
            }
        }

    });

    /* CUSTOMIZATION MAY BE REQUIRED HERE */
    /* TODO: eliminate need for customization; remove hard-coded values */
    var nopacStartpage = document.location.href.indexOf("WWU_NOPAC&startpage=true");
    if (nopacStartpage == -1) {
        // only collect Google Analytics if we are NOT on the nopac start page

        // Google Analytics
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create',  WWU_PRIMO.GA_TRACKING_CODE, WWU_PRIMO.GA_DOMAIN_NAME);
        ga('set', 'anonymizeIp', true);
        ga('send', 'pageview');
    }

})();
