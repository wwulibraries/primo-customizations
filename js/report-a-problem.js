
// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose: this script  injects a "report a problem" link into the Actions menu

 "use strict";
  
WWU_PRIMO.problem = (function() {

        if ($("body").hasClass("EXLFullView")) {
            // add a "Report a problem" link to the Actions menu on the display.do page
            injectReportAProblemLink();
        }

       $(document).on("click", ".EXLTabHeaderButtonSendTo > a ", function() { 
            // add a "Report a problem" link to the Actions menu when the "Actions" link is clicked on from the search/brief results page
            var thisId = $(this).attr('id');
            var thisIdClean = thisId.replace(/[^\w\s]/gi, '');           // remove special characters
            $(this).parent("li").attr("id", thisIdClean);
            injectReportAProblemLink(thisIdClean);
        });

       function injectReportAProblemLink (thisIdClean) {        
            if (typeof thisIdClean === "undefined") {
                // we are on the display.do page; find the only Actions menu;
                var thisId = $("li.EXLTabHeaderButtonSendTo a").attr("id");
                if (thisId) {
                    var thisIdClean = thisId.replace(/[^\w\s]/gi, '');           // remove special characters
                }
            }

            // this was called from the brief/search results page
            var $child_list = $("#" + thisIdClean).parent().find("ol.EXLTabHeaderButtonSendToList, ol.EXLTabHeaderButtonSendTo");
            var child_list_a = $child_list.find(".EXLButtonSendToPermalink a, .EXLButtonSendToOpenUrl a").attr('href');

            // find the href attribute for the exli permalink
            var exli_permalink_generated = WWU_PRIMO.common.recreateExLiPermalink(child_list_a);
            
            // send that value to the recreate function and store it as this variable
            var report_a_problem_url = "http://library.wwu.edu/content/4769?permalink_path=" + exli_permalink_generated;

            // append the generated permalink to our target url, but only if it does not already have one
            var reportaproblem_id = "report-a-problem-" + thisIdClean;
            if($("#" + reportaproblem_id).length == 0) {
                // only create this if it does not already exist
                $child_list.append("<li id='" + reportaproblem_id + "'><a target='_blank' href='" + report_a_problem_url + "'> Report a Problem <span class='rap-bug fa fa-bug'></span></a> </li>");
            }

       }

})();
