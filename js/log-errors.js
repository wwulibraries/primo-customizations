
// Author: David Bass @ w w u . e d u 
// May 2016
// Purpose:: log ajax errors

 "use strict";
 
WWU_PRIMO.errors = (function() {

	var log_error_url= WWU_PRIMO.file_host + "log-error.php";
	

	// hijack the logToServer function so that we can send it to WWU also
   	logToServer = (function() {
        var cached_function = logToServer;

        return function(str) {
            cached_function.apply(this, arguments);   // call the original function

	        // and then add to it by sending the data to wwu
			$.ajax({
			    url: log_error_url,
			    crossDomain: true,
			    type : 'post',
			    data: {msg: str, args: arguments},
			    dataType: 'json',
			});
        };
    }());


	//   	// also try to log other ajax errors 
	// $( document ).ajaxError(function( event, xhr, settings ) {
	// 	if (settings.url) {
	// 		logError( event, xhr, settings  );
	// 	}
	// });


	//  function logError(event, xhr, settings) {	 	
	//  	console.log(" ---");
	//  	var targetUrl = settings.url;
	// 	console.log("settings.url = " + targetUrl);

	// 	var isSearchPage = targetUrl.toLowerCase().indexOf("search.do");
	// 	console.log(isSearchPage);

	// 	if (isSearchPage == -1) {				
	// 		// do not log search.do errors (at least not yet)

	// 		var json_settings = JSON.stringify(settings); 
	// 		var json_event = JSON.stringify(event); 
	// 		var json_request = JSON.stringify(request); 

	// 		$.ajax({
	// 		    url: log_error_url,
	// 		    crossDomain: true,
	// 		    type : 'post',
	// 		    data: {targetUrl: targetUrl,  json_request : json_request, json_event : json_event, json_settings : json_settings},
	// 		    dataType: 'json',
	// 		});
	// 	}
	    
	// }

})();
