
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script modifies the user interface

 "use strict";

WWU_PRIMO.ui = (function() {

    var fn = WWU_PRIMO.fn;
    var mode = WWU_PRIMO.mode;
    var vid = WWU_PRIMO.vid;

   $(window).scrollTop(0);

    // replace the link on error.do with a different link;
    /* TODO: make this customizable */
    $("#exlidErrorSubMessage").html("If the problem persists, please <a href='http://askus.library.wwu.edu/browse.php?tid=23567'>contact us</a>.");

    var searchPage = document.location.href.toLowerCase().indexOf("search.do");
    var newSearchUrl =  $("#btn_new_search").attr("href");    
    $("#onesearch-logo-link").attr("href", newSearchUrl);
        // TODO: add the vid to the end of this url

    $(".EXLReviewsTab a").text("Save/Share");

     $(".EXLAvailabilityCallNumber").hide();     // we will show these after we remove the parens on window load

    if (mode == "Basic") {
        var search_query = WWU_PRIMO.common.getValue("search_field");

        if (searchPage != -1) {
            $("#exlidFacetTile").addClass("basic");
            $("#resultsTileNoId").addClass("basic");
        }    

        // magnifying glass 
        $("div.EXLSearchFieldRibbonFormSubmitSearch").html("<button id='goButton' type='submit'  title='basic search' value='' class='button box-shadow-outset'>  </button>");

        $(".EXLSearchFieldRibbon").append(" <div id='advsearch'><a id='advsearch_href_simple'>Advanced Search</a></div>");   

        $("#advsearch_href_simple").on("click", function() { 
            // append the query to the adv_search button
            var originalAdvSearchUrl = $("#btn_adv_search").attr('href');
            var newestQuery = $("#search_field").val();
            var newAdvSearchUrl = originalAdvSearchUrl.replace(/(vl\(freeText0\)=).*?(&)/,'$1' + newestQuery + '$2');
            window.location = newAdvSearchUrl;
        })
    } 

    if (mode == "Advanced") {
    	$("#exlidAdvancedSearchRibbon #goButton").val("Search").attr("title","advanced search"); 
               var img = WWU_PRIMO.file_host + "/images/onesearch-magnifying-glass.png";
    	$("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormLinks a").html("<img alt='magnifying glass icon' src='" + img + "' /> Basic Search");
    	$("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormLinks a").attr("title","Basic Search");
    	$("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormClearSearch input").attr("value","").attr("title","clear search");
    	// $("#exlidAdvancedSearchRibbon div.EXLSearchFieldRibbonFormClearSearch").append("clear");

    	var query0 = $.trim($("#input_freeText0").val());
    	if (searchPage != -1) {
    		if (query0 == "") {
    			// show the infographic  if there is no query
    			$("#exlidFacetTile").hide();
                                            var img = WWU_PRIMO.file_host  + "/images/onesearch-scopes-sept-2016.png";
    			$("div#exlidHeaderSystemFeedback").prepend("<img id='scopesOverview'  alt='overview of search scopes' src='" + img + "' />");
    		}
    	}
    }

   var looking_for = new RegExp('^Browse');
    var mode_browse = looking_for.test(mode);
    var fn_browse = looking_for.test(fn);

    if  (mode_browse || fn_browse) {
           $("#goButton").val("");      // remove the text from the button
           $(".EXLSearchFieldRibbonFormLinks  a").text("Basic Search");
    }

    if  ((fn == "almaAzSearch") || (fn == "goAlmaAz")) {
           $("#goButton").val("");      // remove the text from the button
    }
    
   $(".EXLClearSimpleSearchBoxButtonClose").css("background-image","none !important");


    // prep the tabs 
    $("div.EXLSearchTabsContainer").addClass("tab-row");
    $("ul#exlidSearchTabs").addClass("tabrow");
    $("li.EXLSearchTab").addClass("tab");
    $("li.EXLSearchTabSelected").addClass("selected");

    // My Account / e-Shelf
    $("div.EXLMyAccountTabsRibbon").addClass("tab-row");
    $("ul.EXLMyAccountTabs").addClass("tabrow");
    $("li.EXLMyAccountTab").addClass("tab");
    $("li.EXLMyAccountSelectedTab").addClass("selected");

    $(".EXLResultTabs").children("li").children("a").addClass("tab-button");      // add a new className to the links to make them look like buttons

    $("li[id*='-ReviewsTab']").hide();
    $("li[id*='-LocationsTab']").hide();
    $("li[id*='-RecommendTab']").hide();

    // $(".EXLTabsRibbon div li").addClass("tab").addClass("tooltip");
    $(".EXLTabsRibbon div li.EXLResultSelectedTab").addClass("selected");

    var displayPage = document.location.href.toLowerCase().indexOf("display.do");
    if (displayPage != -1) {
        // select the Details tab on the display.do page
        $("li[id*='DetailsTab']").addClass("selected");
        $("#Description-1").addClass("shorten");

        // clone the 'back to results' link and add it to the top of the page
        $(".EXLBackToResults").clone().addClass("top").appendTo( "#exlidSearchRibbon" );
    }

    $("#search_field").attr("placeholder","\"Use quotation marks for exact match.\"");


    $(document).on("click", ".email-export", function() { 
        /* find the closest actions button and click it */
        var thisId = $(this).attr("id");
        var str = "$('#" + thisId + "').parents('.EXLResult').find('.EXLTabHeaderButtonSendToList a').click();";
        $(thisId).parents('.EXLResult').find(".EXLTabHeaderButtonSendTo a").click();
    });

    if (searchPage != -1) {
        if  (mode_browse || fn_browse) {
            $("#exlidResultsContainer").addClass("browse");
        }

        $("#exlidClearSearchBox").on("click", function() {
            $("#search_field").focus();     // set the focus to the search field when someone clicks the clear-search button
        });


        /* move the thumbnail to the title column */
        $('td.EXLThumbnail').each(function() {
            var thumbnailHtml = "<div class='thumbnailHtml'>" + $(this).html() + "</div>";
            $(this).parent().find(".EXLResultAvailability").prepend(thumbnailHtml);
            $(this).remove();
        });
        
        /* move the eShelf star over to the left of the item title */
        $("td.EXLMyShelfStar").each(function() {
                var thisContents = $(this).html();
                $(this).parent().find("h2.EXLResultTitle").prepend(thisContents);
                $(this).remove();
        });


        if ((fn == "almaAzSearch") || (fn == "goAlmaAz")) {
            $("#exlidSearchRibbon").addClass("az");
            $("#permalinkPretty").hide();
        }
    }

    if (search_query !== "") {
        // append the query to the adv_search button
        $('#btn_adv_search').attr('href', function() {
            return this.href + '&vid=' + vid + '&vl(freeText0)=' + search_query + "&";
        });
    }


    $("#goButton").hover(
      function() {
        $("#basic-text").show();
      }, function() {
        $("#basic-text").hide();
      }
    );

    $("#advsearch_btn_simple").hover(
      function() {
        $("#adv-text").show();
      }, function() {
        $("#adv-text").hide();
      }
    );

     $(".shorten").shorten( {
            showChars: '500',
            moreText: 'read more',
            lessText: 'read less'
    });



    $("#submit-feedback").click(function() {
        thisObjectId = $(this).data("objectId");
        theComment = $("#feedback-text").val();
        addComment(thisObjectId, theComment);
    });

    reportAddPageToEshelfClick = (function() {
        // add a tick mark when to the  "Add page to e-Shelf" link when clicked
        var cached_function = reportAddPageToEshelfClick;

        return function(str) {
            cached_function.apply(this, arguments); // use .apply() to call it
            $(".EXLFacetSaveToEShelfAction").append("<i class='fa fa-check'></i>");
        };
    }());


    $(window).load(function() {
        $(".EXLAvailabilityCallNumber").each(function() {
            // remove parens from EXLAvailabilityCallNumber 
            var newText = $(this).text().replace(/\(|\)/g, '');
            $(this).text(newText);
            // inherit the status color of the parent 
            var parentClass = $(this).parent(".EXLResultStatusAvailable").attr("class");
            $(this).addClass(parentClass);
        }).fadeIn(500);     
    });

})();
