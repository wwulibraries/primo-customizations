
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script modifies the user interface

 "use strict";

WWU_PRIMO.showHideFacets = (function() {

    $("#exlidResultsContainer").prepend("<div id='hiddenFacetDiv'><span class='fa fa-angle-double-down'></span> &nbsp; Show Facets</div>");          
         // add a hidden div that will replace the facets column when on a narrow screen

    $("#exlidFacetTile").prepend('<div id="compress-facets" class="fa fa-angle-double-left"> &nbsp; Hide Facets</div>');
    //    $("#exlidFacetTile").prepend('<div id="flip-facets" class="fa fa-align-right"> &nbsp; Flip Facets</div>');

    $(document).on("click", "#hiddenFacetDiv", function() {
        $("#hiddenFacetDiv").toggle();
        $("#exlidFacetTile").toggle();
    });

    $(document).on("click", "#compress-facets", function() {
        $("#hiddenFacetDiv").toggle();
        $("#exlidFacetTile").toggle();
    });


    // flip the facets if requested
    $(document).on("click", "#flip-facets", function() {
        $("#exlidResultsContainer").css("flex-direction", "row");
    });


})();
