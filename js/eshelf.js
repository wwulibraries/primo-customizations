// david . bass @ w w u  . e d u
// March 2016
// show which items have been deleted in the e-shelf

 "use strict";

WWU_PRIMO.eshelf = (function() {



	var vid = WWU_PRIMO.vid;

	  if ((RegExp(/basket\.do/i).test(window.location.href))) {
	// loop through each of the user's e-shelf items and note which ones have been deleted

		// if the eshelf is loaded in an iframe, break out of the iframe 
		if (top !== self) top.location.href = self.location.href;


		var deleted_msg = "The record has been deleted from the database";
		var num_deleted = 0;

		// go through each cell and see if it contains the deleted_msg;
		$("table.itemsListTable td.type").each(function() {
			var this_td = $(this).attr('onclick');
			// console.log(this_td);
			if (this_td.indexOf(deleted_msg) > -1) {
				num_deleted++;
				$(this).addClass("strike").css("color","red");
				// $(this).prev().find("input:checkbox").attr('checked', true);	// check these by default, to make them easier to delete/remove;

				var author = $(this).siblings(".author").text();
				var title = $(this).siblings(".title").text();

				$(this).siblings( ).not(".itemsList_left").attr("onclick", "").addClass("newSearch");
				$(this).parent("tr").data({"author": author, "title": title});

			}	
		});

		$(".newSearch").on("click", function() {
			var $parent = $(this).parent();
			$("tr").removeClass("selected");
			$parent.addClass("selected");
			var author = $parent.data("author");
			var title = $parent.data("title");
			var newSearchUrl = "/primo_library/libweb/action/dlSearch.do?embed=true&institution=01ALLIANCE_WWU&mode=Advanced&vl(freeText0)=" + author + "&query=creator,exact," + author + "&vl(freeText1)=" + title + "&query=title,exact," + title + "&vid=" + vid + "&search_scope=All&tab=everything";
			$("#demoLibId").attr("src", newSearchUrl);

		});

		if (num_deleted > 0) {
			// add a message at the top telling the user how many have been deleted, and to prompt them to remove them;
			var msg = num_deleted + " of the items in your e-shelf no longer exist and have been marked in <span class='strike' style='color:red'>red</span>.  Click the author or title to search for them again, and then add them to your e-shelf again, and then remove the outdated version.";
			$("table.folderName.EXLEShelfFolderContentsTable tr td:first").append(msg);		
		}

	  }

})();
