
// Author: David Bass @ wwu.edu 
// Date: November 2015
// Purpose: this script  adds estimated result counts to the list of scopes (when they are presented as tabs)

 "use strict";

WWU_PRIMO.scopeTabs = (function() {

    var vid = WWU_PRIMO.vid;
    var mode = WWU_PRIMO.mode;
    var institution = "WWU";
    var tab_count_url = WWU_PRIMO.file_host + "/scope-count.php";

    function populateTabs() {

        function getCount(tabId, ajax_query, scope, institution) {
            ajax_query = ajax_query.replace(/%2C/g, '+');       // replace comma with plus
            var count_url = tab_count_url + "?i=" + institution + "&s=" + scope + ajax_query + "&callback=?";
            var thisId = "#" + tabId;

            $.getJSON(count_url, function(data) {
                $(thisId + " span.spin-container .spin").fadeOut("fast");
                $(thisId + " span.spin-container").addClass("fadein").text("(" + data.count + ")");
            }).done(function( json ) {
                // console.log(json); 
            }).fail(function( jqxhr, textStatus, error ) {
                var err = textStatus + ", " + error;
                console.log( "Request Failed: " + err );
            });
        }


        var current_count = 0;

        var searchPage = document.location.href.toLowerCase().indexOf("search.do");
        if (searchPage != -1) {
            current_count = $.trim($("#resultsNumbersTile h1 em:first").text());        // global variable
            var api_query = "";
            var search_query = "";

            if (mode == "Advanced") {

                var current_scope = $.trim($("#exlidSearchTabs li.selected span").attr("id"));
                current_scope = current_scope.replace("defaultScope","");

                $("[id^=input_freeText]").each(function(index) {
                    var thisCounter = index + 1;
                    var thisValue = $(this).val();
                    if (thisValue !== "") {
                        search_query = $(this).val();
                        var thisScopeId = "#exlidInput_scope_" + thisCounter;
                        var thisScope = $(thisScopeId + " option:selected").val();
                        var thisOperatorId = "#exlidInput_precisionOperator_" + thisCounter;
                        var thisOperator = $(thisOperatorId + " option:selected").val();
                        api_query += "&q[]=" + thisScope + "," + thisOperator + "," + encodeURIComponent(search_query);
                    }
                });
            } else {
                var current_scope = $.trim($(".EXLSelectedScopeId").val());
                search_query = $("#search_field").val();
                api_query = "&q=any,contains," + encodeURIComponent(search_query);
            }


            if (search_query !== "") {
                var other_scopes = {};      // create empty object

                $("#exlidSearchTabs li:visible").each(function() {
                    // for each of the visible scope tabs, insert a spinner span to indicate activity when doing the background searches
                    // wwu has an invisible tab named "default" to handle links created before we introduced tabs
                    var this_id = $(this).attr("id");
                    $(this).find("a").append(" <span class='spin-container'> &nbsp; &nbsp; <span class='spin'> &nbsp; </span> </span>");

                    var this_tab_selected = $(this).hasClass("selected");
                    if (this_tab_selected) {
                        // do not use Ajax to find count for this tab; just copy it from the html (current_count)
                        $("#" + this_id +" span.spin-container").addClass("fadein").text("(" + current_count + ")");
                    } else {
                        // add this id and the span id to an array, that we will process later
                        var this_scope_value = $(this).find("span").attr("id");                                 // what is the value of this id?
                        this_scope_value = this_scope_value.replace("defaultScope", "");        // remove the word "defaultScope" from the value before we send it to the php script
                        other_scopes[this_id] = this_scope_value;                                                       // add these values to the object, so that we can run through them later
                    }
                });

                $.each(other_scopes, function (key,value) {
                    // go through the list of other_scopes and get their result count via an ajax request
                    var tab_id = key;
                    var scope_name = value;
                    // console.log(key + " : " + value);
                    getCount(tab_id, api_query, scope_name, institution);                    
                });
            }
        }
    }


    var populateTabs = populateTabs();


    $("li.tab")
          .mouseenter(function() {
            var thisId = $(this).attr("id");
            var tabtipId = "#" + thisId + "_tabtip";
            $(tabtipId).show();
        })
          .mouseleave(function() {
            var thisId = $(this).attr("id");
            var tabtipId = "#" + thisId + "_tabtip";
            $(tabtipId).hide();
    });



    $(window).load(function() {

        // add mouseover tooltips to the scope tabs
        $("#exlidSearchTabs li.tab:visible").each(function(index) {
            var thisTitle = $(this).find("a").attr("title");
            $(this).find("a").attr("title", "");    // erase the title to prevent it from appearing

            var thisId = $(this).attr("id");
            var position = $(this).position();
            var x = position.left;
            var tabtipId = thisId + "_tabtip";
            var tooltip = "<div style='position: relative; left:" + x + "px;' id='" + tabtipId + "' class='tabtip showme'>" + thisTitle + "</div>";
            $(".EXLSearchFieldRibbonFormFields").append(tooltip);
        });
    });


})();