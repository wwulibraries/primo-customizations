
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds the Springshare chat functionality to Primo

 "use strict";

    $(window).load(function() {
        /* 25aug2014 - David Bass - have to clone this for use in the "Didn't find" popup */
        //  $("#libchat_btn_widget").clone().appendTo("#chat_with_librarian");
        $("#exlidSearchBanner").fadeIn();
        $("#libchat_btn_widget a").attr("target", "_blank");

    });


WWU_PRIMO.chat = function() {

       /* LibGuides Version 2 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

       if (WWU_PRIMO.LIBANSWERS_V2_CHAT_HASH) {
          var chat_html = "<div id='chatDiv'><div id='chatDivLinkContainer'><div id='libchat_117f795f06117e9f4be68a41d4c52c60'></div></div></div>";

          if (document.getElementById("exlidSearchBanner")) {
            $("#exlidSearchBanner").html(chat_html);
          }

          if (document.getElementsByClassName("EXLSearchFieldRibbon")) {
            $(".EXLSearchFieldRibbon").append(chat_html);
          }

        var libguide_script_url = '//v2.libanswers.com/load_chat.php?hash=' +  WWU_PRIMO.LIBANSWERS_V2_CHAT_HASH;        

         // loadjscssfile(libguide_script_url, "js");
            $.getScript(libguide_script_url, function(data, textStatus){
            });

       }

    /* LibGuides Version 1 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */

       if (WWU_PRIMO.LIBGUIDES_V1_CHAT_HASH) {
            if (document.getElementById("exlidSearchBanner")) {
                document.getElementById("exlidSearchBanner").innerHTML = "<div id='chatDiv'><div id='chatDivLinkContainer'><div id='libchat_btn_widget'></div></div></div>";
            }

            if ($("#exlidAdvancedSearchTile")) {
               $("#exlidAdvancedSearchTile").append("<div id='chatDiv'><div id='chatDivLinkContainer'><div id='libchat_btn_widget'></div></div></div>");
            }


            /* libchat_btn needs to be in the global scope */
            var libchat_btn = {
                iid: "123",
                key: "see your libguides account for this",
                domain: "askus.library.example.edu",
                splash: "Welcome to Example Libraries Chat!",
                wait: "Please wait... A librarian will connect shortly!",
                error_offline: "Sorry it doesn't appear any librarians are online... Please try again later.",
                button_online: "//library.example.edu/images/chat-online.png",
                button_offline: "//library.example.edu/images/chat-offline.png",
                question: "Please enter question below:",
                star_ratings: true,
                star_text: "Please rate this chat:",
                depart_id: "987"
                };


            $.getScript('//libanswers.com/js/chat_load_client.js', function(data, textStatus){
                // console.log("chat client loaded");
            });
        }    
}();
