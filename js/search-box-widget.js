/* David Bass - April 2016 - custom search widget */

var wwu_primo_search_widget_go = (function() {

	jQuery(document).ready(function() {

		var placeholders = wwu_primo_search_widget.placeholders;
		var cycle = wwu_primo_search_widget.cycle;
		var delay = wwu_primo_search_widget.delay;

		var cycle = function() { 
		    var placeholder = placeholders.shift();
		    // $('#search_field').attr('placeholder',placeholder);
		    $("#search_tip").fadeOut().text("").text(placeholder).fadeIn();
		    placeholders.push(placeholder);
		    setTimeout(cycle,delay);
		};

		if (cycle == true) {
			cycle();
		} else {		
			// show a random search tip
			var random_placeholder = placeholders[Math.floor(Math.random() * placeholders.length)];
			$("#search_tip").text(random_placeholder);
		}

		jQuery("#submit-to-primo").on("submit", function(event) {
		 	event.preventDefault();
			searchPrimo();
		});

		function searchPrimo() {
		 	var query = "";
		 	var searchURL = "";
		 	var domain = "http://onesearch.library.wwu.edu";

			  var tempQueryValue = jQuery("#search_field").val();

			var searchURL = wwu_primo_search_widget.submit_to;

			$("#submit-to-primo input").each(function() {
				var temp_name = $(this).attr("name");
				var temp_value = $(this).val();
			 	searchURL += "&" + temp_name + "=" + temp_value;
			});

			if (tempQueryValue != "") {
				var tempQueryValueSimple = tempQueryValue.replace(/'/g,"\'");
				tempQueryValueSimple = tempQueryValueSimple.replace(/ \* /g, " ");
				tempQueryValueSimple = tempQueryValueSimple.replace(/ \? /g, " ");
				tempQueryValueSimple = tempQueryValueSimple.replace(/,/g,' ');
				var query_encoded = encodeURIComponent(tempQueryValueSimple);
				query = "&query=any,contains," + query_encoded;
			} else {
				query = "&query=";
			}

			searchURL +=  query;

			// var finalUrl =  encodeURI(preSearch + searchURL);
			location.href = searchURL;
			return false;
		}

	});

})();