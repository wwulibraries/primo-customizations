
// Author: david.bass @ wwu.edu 
// Date: November 2015
// Purpose: this script adds checkboxes to the list of facet links, which allows for multiple facets to be applied simultaneously.
// This script also replaces the "show more" popup/ modal dialog with an expanded list of facets for each section

 "use strict";

function determineCheckboxState(cb) {
    // determine the state of a checkbox (checked, indeterminate, unchecked);
    // http://jsfiddle.net/chriscoyier/mGg85/2/
    if (cb.readOnly) {  
        cb.checked=cb.readOnly=false;        
    } else if (!cb.checked) {
        cb.readOnly=cb.indeterminate=true;
    }
}

WWU_PRIMO.facets = (function() {

    $(".EXLFacetsDisplayMore").each(function(){
        $(this).html("<button class='showHideFilters'>show/hide filters</button>");
    });

    $(".showHideFilters").click(function() {
        $(this).parent().siblings(".EXLAdditionalFacet").toggle();
        // TODO: try these:
            //    $('.element').fadeToggle('slow'); 
            //  $('.element').slideToggle('slow');
     });

    var facets = "";
    $("#facetList").each(function() {
        facets += $(this).html();
    });

    $("#exlidFacetTile").prepend("<div class='filterButtonDiv'><button type='button' tabindex='0' class='filterButton'>Apply Filter(s)</button></div>");
    $("#facetList").append("<div class='filterButtonDiv'><button type='button' tabindex='0'  class='filterButton'>Apply Filter(s)</button></div>");

    $("ol.EXLFacetsList").each(function() {
        var thisGroupIdNumber = "";
        var thisGroupId = $(this).attr("id");
        if (thisGroupId) {
            if (thisGroupId == "exlidFacetSublistX") {
                thisGroupIdNumber = 0;
            } else {
                thisGroupIdNumber = thisGroupId.replace(/\D/g,'');
                thisGroupIdNumber++;    
            }
        }

        $(this).find("li.EXLFacet a").each(function(i) {
            // put a checkbox before each facet link;
            var theURL = $(this).attr("href");
            var fctN = WWU_PRIMO.common.GetParameter(theURL,"fctN");      // extract the value from that string
            var fctV = WWU_PRIMO.common.GetParameter(theURL,"fctV");      // extract the value from that string
            var thisId = thisGroupIdNumber + "_" + i;

            // rfnIncGrp=show_only

            theURL = theURL.replace('fctN','mulIncFctN');
            theURL = theURL.replace('fctV','fctIncV');
            theURL = theURL.replace('rfnGrp','rfnIncGrp');
            theURL += "&rfnGrpCounter=" + thisGroupIdNumber;
            $(this).attr("href", theURL);       // replace the link facet querystring variables with the group equivalents

            $(this).parent().prepend("<input type='checkbox' class='facetcheckbox' tabindex='0' onclick='determineCheckboxState(this)' name='" + fctN + "' value='" + fctV + "' data-group='" + thisGroupIdNumber + "' id='facetcheckbox_" + thisId + "'> <label for='facetcheckbox_" + thisId + "'  role='checkbox' aria-checked='false' tabindex='0'  title='check once to filter by this term; check twice to exclude; and a 3rd time to un-check'></label> ");
        });
    });


    /* because  each browser renders the indeterminate state differently, let's show a minus to indicate this will be excluded */
    $(".facetcheckbox").on("click", function() {
            var isIndeterminate = $(this).prop("indeterminate");       
            var isChecked = $(this).prop("checked");

            if (isIndeterminate) {
                $(this).attr("aria-checked", "mixed");
                $(this).parent().find("a").addClass("strike").attr("title","exclude this facet from your results");
             } else if (isChecked) {
                $(this).attr("aria-checked", "true");
                $(this).parent().find("a").removeClass("strike").attr("title", "results will be filtered by this facet after you 'Apply filters'");                        
            } else {
                $(this).attr("aria-checked", "false");
                $(this).parent().find("a").removeClass("strike").attr("title", "click to filter data by this facet").after("<span class='EXLHiddenCue'>press Enter or Return to apply this facet</span>");
            }
     });

    var thisFilterValue = "";

    $(".filterButton").on("click", function() {
        // when someone clicks on the 'apply filter(s)' button, determine which checkboxes are checked, and then redirect to the new URL based on those filters

        $("li.EXLFacet input.facetcheckbox").each(function(i) {
            // append these filters to the end of the current URL and go there;
            var counter = i + 1;

            var isIndeterminate = $(this).prop("indeterminate");       
            var isChecked = $(this).prop("checked");

            var thisName = $(this).attr("name");
            var thisValue = $(this).attr("value");
            var thisGroup = $(this).data("group");

            if (isChecked) {
                thisFilterValue += "&ct=facet&mulIncFctN=" + thisName + "&fctIncV=" + thisValue;
                thisFilterValue += "&rfnIncGrp=" + thisGroup + "&rfnGrpCounter=" + thisGroup;
            }

            if (isIndeterminate) {
                thisFilterValue += "&ct=facet&mulExcFctN=" + thisName + "&fctExcV=" + thisValue;
                thisFilterValue += "&rfnExcGrp=" + thisGroup + "&rfnGrpCounter=" + thisGroup;
            }
        });

        var currentUrl = window.location.href;
        var newUrl = currentUrl.replace("initialSearch=true", "");
        newUrl = newUrl.replace("permalink_key=", "original_plink_key=");   // if someone applies facets after using a permalink, we should remove the permalink key from the url so that it doesn't appear again
        newUrl = newUrl + thisFilterValue;
        window.location = newUrl;
    });


    /*
         23 Sept 2015 - David Bass
         add an "OR" between combined .EXLRemoveRefinement, and an "AND" between each different set
         for instance, 
              Refined by: Format: Articles  Print Books             Language: English  
         would become:
              Refined by: Format: Articles OR  Print Books AND   Language: English  
    */

    var refinementCategories = $(".EXLRemoveRefinement").length - 1;
    $(".EXLRemoveRefinement").each(function(index) {
        // add "AND" between each span of this class - except for the last one
        //   console.log(index + " / " + refinementCategories);
        if (index < refinementCategories) {
            $(this).after(" AND ");
        }

        var numFacets = $(this).find("a").length;
        // console.log("numFacets = " + numFacets);

        $(this).find("a").each(function(index) {
            var theClass = $.trim($(this).attr("class"));
            // console.log("theClass = " + theClass);
            // console.log("index = " + index);
            var msg = null;

            // put "OR" in between them (by prepending "OR" for all but the first one), unless it is an exclusion, and then say " AND NOT ".
            if ((index > 0) && (numFacets > 1)) {
                // if we are not on the first facet, and there are more than one facets
                if (theClass == "EXLExcludedElement") {
                    msg = " AND ";      /* and not */
                } else {
                    msg = " OR ";
                }

               $(this).before(msg);
               $(".EXLRefinementRibbonWithExclude div a").css("border-left", "none");       // remove the left border / divider
            }

        });
    });



    var facet_count = $('#facetList').find("li.EXLFacet").find("a").length;
    $('#facetList').find("li.EXLFacet").find("a").each(function(index) {
        var facetCounter = index + 1;
        var facetOrderValue = "facet " + facetCounter + " of " + facet_count;
        $(this).data("facetOrder", facetOrderValue);
        $(this).addClass("facetLink");
        $(this).attr("title", "click to filter data by this facet");
        $(this).attr("tabindex", "0");
    });

    /* TODO: add showme tour feature to facets */
    var showFacetTour = "<span id='facetTour' class='tabtip showme'>Click once to include, twice to exclude,  and a 3rd time to clear</span>";
    $("li.EXLFacet").first().prepend(showFacetTour);

})();
