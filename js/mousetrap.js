 "use strict";

WWU_PRIMO.mousetrap = (function() {

    Mousetrap.bind('ctrl+/', _focusSearch);
    Mousetrap.bind('ctrl+.', _focusPermalink);
    Mousetrap.bind('ctrl+,', _focusFilter);

    $("#search_field").addClass("mousetrap").append('<span class="EXLHiddenCue">press CTRL + / to focus on this field.</span>');
    $("#facetcheckbox_1_0").addClass("mousetrap").append('<span class="EXLHiddenCue">press CTRL + . to focus on the first facet/filter.</span>');
    $("#generate-permalink").addClass("mousetrap").append('<span class="EXLHiddenCue">press CTRL + , to focus on the permalink button.</span>');

    function _focusSearch() {
        // console.log("set focus to search input");
        $("#search_field").addClass("new-item").focus();
    }

    function _focusFilter() {
        // console.log("set focus to first filter/facet checkbox");
        $("#facetcheckbox_1_0").focus().scrollTop();
        $("#facetcheckbox_1_0").parent("li").addClass("new-item");
    }

    function _focusPermalink() {
        // console.log("set focus to permalink button");
        $("#generate-permalink").addClass("new-item").focus();
    }

})();    