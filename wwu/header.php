<?php
  $server_name = $_SERVER["SERVER_NAME"];
  $doc_root = $_SERVER['DOCUMENT_ROOT'];
  $primo_root = $doc_root . "/primo";
  $dist_root = $primo_root . "/dist";

  require("../utils.php");

?>

<?php # if (substr_count($_SERVER[‘HTTP_ACCEPT_ENCODING’], ‘gzip’)) ob_start(“ob_gzhandler”); else ob_start(); ?>

<!-- for details on this interface, go to: https://bitbucket.org/wwulibraries/primo-customizations -->
<!--
  <script>
    // Check to see if user has seen Quick Search pop-up window in same session
   var isFirst = window.sessionStorage.getItem("FIRSTVISIT");
   var pCookie = getPCookie("primofirst");
   var urlp = document.referrer;
  //If user has not seen pop-window in same session store visit
  if((isFirst == null || isFirst == 'undefined') && pCookie == null && urlp.indexOf("alliance-primo-pds") == -1 && urlp.indexOf("onesearch.library.wwu.edu") == -1) {
      window.sessionStorage.setItem("FIRSTVISIT", "YES");
      document.cookie = "primofirst=true";
      window.location= "http://" + window.location.hostname + "/primo_library/libweb/action/login.do?loginFn=signin&vid=WWU&targetURL=" + document.URL;
          }
  </script>
-->

<noscript>JavaScript is required</noscript>

<script>

    var WWU_PRIMO = { 
      institution : "01ALLIANCE_WWU",
      file_host : "//<?php echo $server_name; ?>/primo/",    /* where are these files hosted?  */ 
      GA_TRACKING_CODE : "UA-3067340-13",
      GA_DOMAIN_NAME : "wwu.edu",
      GOTO_PERMALINK_URL : "http://library.wwu.edu/goto/?",           /* depending on your redirector script, you may not need the question mark */ 
      CREATE_PERMALINK_URL : "http://libweb1.library.wwu.edu/primo/permalinks/create.php",  
      error_email : "onesearch@wwu.edu",             /* what email address should we show the end-user to ask for help?  */ 
      LIBANSWERS_V2_CHAT_HASH : "117f795f06117e9f4be68a41d4c52c60",   /* LibAnswers Chat v2  */ 
      expand_results_for : "everythingandmore",     /* do you have any views that have expand_my_results (pcAvailabiltyMode=true) enabled by default? */
      PRODUCTION : true,
    };
</script>

<?php 


  load_file('/primo/wwu/css/style.css');
  load_file('/primo/wwu/css/tabs.css');
  load_file('/primo/wwu/css/bootstrap.css');
  load_file('/primo/wwu/css/show-hide-facets.css');
  load_file('/primo/wwu/css/spinner.css');
  load_file('/primo/wwu/css/permalink.css');
  load_file('/primo/wwu/css/payfines.css');
  load_file('/primo/wwu/css/citation-trails.css');
  load_file('/primo/wwu/css/no_pds_login.css');
  load_file('/primo/wwu/css/keep-facets.css');
?>
<!-- thanks to http://terrillthompson.com/blog/474 -->

<div id='newMenu'>
  <div id='left'>
    <a title='start a new search' href="http://onesearch.library.wwu.edu" id='onesearch-logo-link'><img id='logo' src='//libweb1.library.wwu.edu/primo/images/OneSearch-logo.png' alt='OneSearch logo' /></a>
  </div> 
  <div id='right'>
          <a href='http://library.wwu.edu/' title='Library home page' id='libraryLogoLink'><img src='//libweb1.library.wwu.edu/primo/images/Logo-WesternLibraries-RGB-REV.png' alt='Western Libraries logo' id='westernLibrariesLogo' /></a>
    <div id='mainMenuLinks' role='navigation' aria-label='Main menu' tabindex='-1'>
      <ul id='kba_nav' class='menubar root-level' role='menubar'>
        <li tabindex='0' class='menu-parent' role='menuitem' aria-haspopup='true' title='Feedback'>
            <a id='feedbackLink' href='http://goo.gl/forms/A4bCPmcJVi'> Feedback</a>
        </li>
        <li class='menu-parent' role='menuitem' aria-haspopup='true' title='Search Menu'>
          Search Tools
          <ul class='submenu menu' role='menu' aria-hidden='true'>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' id='btn_new_search' title='new search' href='/primo_library/libweb/action/search.do?menuitem=0&fromTop=true&fromPreferences=false&fromEshelf=false&tab=everything'>
                <span class='fa fa-search  fa-flip-horizontal'></span> Start a New Search</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' id='btn_adv_search' href='/primo_library/libweb/action/search.do?mode=Advanced&ct=AdvancedSearch&dscnt=0&tab=everything&fn=search' title='advanced search'>
                <span class='fa  fa-search-plus  fa-flip-horizontal'></span> Advanced Search</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' id='browseSearchUrl' href='/primo_library/libweb/action/search.do?fn=showBrowse&mode=BrowseSearch&dscnt=1&institute=&fromLogin=true&initializeIndex=true' title='browse search'>
                <span class='fa  fa-eye'></span> Browse Search</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem' title='databases' href='http://library.wwu.edu/databases'>
                <span class='fa fa-database'></span> Databases</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' title='A-Z Journals' id='azJournalsUrl' href='/primo_library/libweb/action/dlSearch.do?&institution=WWU&azSearch=true&fn=almaAzSearch&query=facet_atoz%2Cexact%2CA'>
                <span class='fa  fa-newspaper-o'></span> A-Z Journals</a> <!-- fa-leanpub -->
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem' title='WorldCat' href='' id='Worldcat-Menu-Link' target="_blank">
                <img src='//libweb1.library.wwu.edu/primo/images/worldcat.png' alt="Worldcat icon" /> Worldcat</a> <!-- fa-leanpub -->
            </li>        
        </ul>
        </li>

        <li tabindex='0' class='menu-parent' role='menuitem' aria-haspopup='true' title='Help Menu'>
          Help
          <ul class='submenu menu' role='menu' aria-hidden='true' id='helpSubMenu'>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem' title='search tips' href='http://libguides.wwu.edu/onesearch'>
                <span class='fa  fa-lightbulb-o'></span> OneSearch Tips</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem' href='http://libguides.wwu.edu/' title='library guides'>
                <span class='fa fa-map-marker'></span> Library Guides</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem' href='http://bit.ly/2aZLW8O' target='faqs' title='Frequently Asked Questions'>
                <span class='fa  fa-question'></span> FAQs</a>
            </li>
            <!--li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem' id='featureTour'>
                <span class="fa fa-toggle-on"></span> Feature Tour
              </a>
              </li-->
          </ul>
        </li>

        <li tabindex='0' class='menu-parent' role='menuitem' aria-haspopup='true' title='Account Menu'>
          <span id='myAccountMenuHeading'></span>
          <ul class='submenu menu' role='menu' aria-hidden='true' id='myAccountSubMenu'>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' title='my account' href='/primo_library/libweb/action/myAccountMenu.do?'>
                <span class='fa fa-user'></span> My Account</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' href='/primo_library/libweb/action/basket.do?fn=display&fromUserArea=true&fromPreferences=false' title='eShelf'>
                <span class='fa fa-star'></span> eShelf</a>
            </li>
            <li role='menuitem' class='menu-item' tabindex='-1'>
              <a class='submenuitem exlibris' id='loginLink' title='' href=''>
                <span class='fa fa-unlock-alt'></span>
                <span id='loginLink-tooltip'></span></a>
            </li>
          </ul>
        </li>
        <li id='username'></li>
      </ul>
    </div>
</div>
</div>



  
<!-- Modal -->
<div  class="modal fade" id="didntFindModal" role="dialog" aria-labelledby="didntFindModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="didntFindModalLabel">Additional Search + Request Options</h4>
      </div>
      <div class="modal-body">
        <div id='repeat-in-worldcat'>
          <a target="_blank" href=''  id='repeat-in-worldcat-link'>Repeat Search in WorldCat</a>
        </div>
        <br>
        <div id='illiad_request_form'>
            Request an item <a id='illiad_link' title='request item'>via ILLiad</a>
        </div>
        <br>
        <!--div id='include-results-extra-time'>
              <input type="checkbox" name="pcAvailabiltyMode" value="on"  id="extra-time"> Include search results that require additional delivery time.
        </div-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php 
    $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    echo "\n<!-- Header - Process Time: {$time} -->";
?>
