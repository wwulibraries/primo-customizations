<?php
  $server_name = $_SERVER["SERVER_NAME"];
  $doc_root = $_SERVER['DOCUMENT_ROOT'];
  $primo_root = $doc_root . "/primo";
  $dist_root = $primo_root . "/dist";

  require("../utils.php");
?>

<?php # if (substr_count($_SERVER[‘HTTP_ACCEPT_ENCODING’], ‘gzip’)) ob_start(“ob_gzhandler”); else ob_start(); ?>

<!--begin footer-->
  <div class="EXLFooterLinksContainer">
    <ul class="EXLFooterLinksList1">
      <li class="EXLFooterFirstLink"><a href="http://library.wwu.edu/">Library Home</a></li>
      <li><a href="http://www.wwu.edu/">Western Washington University Home</a></li>
      <li class="EXLFooterLastLink"><a href="http://askus.library.wwu.edu/">Ask Us!</a></li>
      <li >Powered by ExLibris Primo</li>
      <li>Copyright &copy; 2016</li>
      <!--<li class="EXLFooterLastLink"><a href="#">Accessibility Statement &amp; Disclaimer</a></li>-->
    </ul>
  </div>

<?php

  load_file('/primo/js/common.js');
  load_file('/primo/js/bootstrap.js');
  load_file('/primo/js/mousetrap-lib.js');
  load_file('/primo/js/show-hide-facets.js');
  load_file('/primo/js/jquery.shorten.js');
  load_file('/primo/js/css.js');
  load_file('/primo/js/ui.js');
  load_file('/primo/js/header.js');
  load_file('/primo/js/chat.js');
  load_file('/primo/js/scope-tabs.js');
  load_file('/primo/js/facets.js');
  load_file('/primo/js/uncommon.js');
  load_file('/primo/js/permalink.js');
  load_file('/primo/js/report-a-problem.js');
  load_file('/primo/js/analytics.js');
  load_file('/primo/js/jump-to-page.js');
  load_file('/primo/js/tooltips.js');
  load_file('/primo/js/more-options.js');
  load_file('/primo/js/did-you-mean.js');
  load_file('/primo/js/payfines.js');
  load_file('/primo/js/citation-trail.js');
  load_file('/primo/js/mousetrap.js');
  load_file('/primo/js/eshelf.js');
  load_file('/primo/js/keep-selected-facets.js');

?>

<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type='text/javascript' src='/primo_library/libweb/javascript/jQuery.PRIMO.min.js'></script>

<?php 
    $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    echo "\n<!-- Footer - Process Time: {$time} -->";
?>
<!--end footer-->