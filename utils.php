<?php

  function load_file($filename) {
      $path_parts = explode("/", $filename);    // . css permalink.css
      $the_filename = end($path_parts);           //  permalink.css
      $filename_parts = explode(".", $the_filename);   // style css
      $file_extension = end($filename_parts);   // css
        echo "\n<!-- //" . $GLOBALS['server_name'] . $filename . " -->";
        if ($file_extension == "css") {
            echo "\n<style>";
            echo get_content($filename);
            echo "\n</style>";
        }
        if ($file_extension == "js") {
            echo "<script>\n";
            echo get_content($filename);
            echo "\n</script>";
        }
}


/* gets the contents of a file if it exists, otherwise grabs and caches */
function get_content($filename) {
  $full_filename = $GLOBALS['doc_root'] . $filename;
  if (file_exists($full_filename)) {
      $path_parts = explode("/", $full_filename);    // . css permalink.css
      $the_filename = end($path_parts);           //  permalink.css
      $filename_parts = explode(".", $the_filename);   // permalink css
      $file_extension = end($filename_parts);   // css
      $minimized_filename =  $GLOBALS['dist_root'] . "/" . $file_extension . "/" . $filename_parts[0] . ".min." . $filename_parts[1];     // this assumes the filenames are always like css/filename.css (that there is only one period)
      $minimize_file_url = "http://dev0.library.wwu.edu/min/?f=" . $filename;   # http://dev0.library.wwu.edu/min/?f=/primo/js/keep-selected-facets.js

      // echo "\n\n minimized_filename = " . $minimized_filename;
      // echo "\n\n minimize_file_url = " . $minimize_file_url;

      if (file_exists($minimized_filename)) {
          # the minimized file exists; see if it's older than the non-minimized version
          $date_changed_file = filemtime($full_filename);
          $date_changed_minified_file = filemtime($minimized_filename);
          if ($date_changed_file > $date_changed_minified_file) {
              # the original file has been updated, so we need to minify and save it again
              $content = get_url($minimize_file_url);
              file_put_contents($minimized_filename,$content);
              //echo 'retrieved fresh from '.$minimize_file_url.':: '.$content;
              return $content;
          } else {
             # the min version is up-to-date; load that one;
              return file_get_contents($minimized_filename);
          }
        } else {
          # the minimized version does not exist yet; create it
          $content = get_url($minimize_file_url);
          file_put_contents($minimized_filename, $content);
          return $content;
        }
    } else {
      return "\n <!-- ERROR - " . $filename . " does not exist.-->";
    }
  }




/* gets content from a URL via curl */
function get_url($url) {
  $ch = curl_init();
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); 
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,5);
  $content = curl_exec($ch);
  curl_close($ch);
  return $content;
}


?>