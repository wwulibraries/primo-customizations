README

We created a MySQL database named "permalinks", and then created a table like this:

	CREATE TABLE `primo_permalinks` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `created` datetime DEFAULT NULL,
	  `nsrid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
	  `path` text COLLATE utf8_unicode_ci NOT NULL,
	  `version` TINYINT DEFAULT '0',
	  `modified` datetime DEFAULT NULL,
	   PRIMARY KEY (`id`),
	   UNIQUE KEY (`nsrid`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci

and then created a MySQL user that only has INSERT and SELECT access to the table below.

	CREATE USER 'primo_perma_user'@'localhost' IDENTIFIED BY 'password-goes-here';

	GRANT INSERT, SELECT PRIVILEGES ON permalinks.primo_permalinks TO 'primo_perma_user'@'localhost';

	FLUSH PRIVILEGES;


and then, create a file named "db-connection.php" in this folder, and populate the following variables:

	<?php

	$dbServer = "localhost";
	$dbName = " ";
	$dbUserName = " ";
	$dbPassword = " ";

	?>


Finally, our permalinks look like http://library.wwu.edu/goto/?56d4dd0805feb

So, if you want your permalinks to have a similar structure, create a folder named "goto" on your web server, and create an "index.php" file that has the following contents:

	<?php

	    include( $_SERVER['DOCUMENT_ROOT'] . "/primo/permalinks/lookup.php");
	  
	?>
