<?php


$destination_domain = "http://onesearch.library.wwu.edu";
$path = "";

if (isset($_SERVER['QUERY_STRING'])) {
	$nsrid = $_SERVER['QUERY_STRING'];

	if (preg_match('/\W/', $nsrid)) {
		// make sure the nsrid only contains a-Z and 0-9
		echo "Invalid id.";
		exit();
	}

} else {
	echo " Error: Missing permalink id. ";
	exit();
}

include($_SERVER['DOCUMENT_ROOT'] . "/primo/permalinks/db-connection.php");

try {

	$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);
	$stmt = $pdo->prepare('SELECT path FROM primo_permalinks WHERE nsrid = :nsrid LIMIT 1');
	$stmt->execute(array('nsrid' => $nsrid));
	$path = $stmt->fetchColumn();

	if ($path != "") {
		// if the tab value is not defined, default to the 'everything' tab
		$tab_empty = strrpos($path, "tab=&");
		if ($tab_empty !== false) {
			$path = str_replace('tab=&', 'tab=everything&', $path);    
		}

	        	$destination = $destination_domain . "/primo_library/libweb/action/dlSearch.do?" . $path . "&permalink_key=" . $nsrid;
	       	header ('HTTP/1.1 301 Moved Permanently');  
	       	header ('Location: ' . $destination );
	 } else {
	        	$destination = $destination_domain;
	        	echo "An error occurred; we could not find that permalink. <br> Please continue to <a href='" . $destination . "'>" . $destination . "</a>";
	        	# TODO: notify admin of this error;
	 }

	$pdo = null;
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}


?>