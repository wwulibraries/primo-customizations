<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, OPTIONS');
header('Access-Control-Allow-Headers: EXLRequestType, Origin, Content-Type, Accept');

function nsrid_exists($nsrid, $pdo) {

	try {
		$stmt = $pdo->prepare('SELECT nsrid FROM primo_permalinks WHERE nsrid = :nsrid');
		$stmt->execute(array('nsrid' => $nsrid));
		if ($stmt->fetchColumn() > 0) {
			// this nsrid already exists; try again;
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
	}

}

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

	if(isset($_GET['query'])) {
		// this is a new key-based (lookup) permalink;

		include($_SERVER['DOCUMENT_ROOT'] . "/primo/permalinks/db-connection.php");

		$version = 1;			// Feb 2016

		try {

			$pdo = new PDO('mysql:host='.$dbServer.'; dbname='.$dbName, $dbUserName, $dbPassword);

			$path = $_GET['query'];
			$path = filter_var($path, FILTER_SANITIZE_STRING);

			do {
				$nsrid = uniqid();
		      	}
		      		while (nsrid_exists($nsrid, $pdo));


		      	$now = date('Y-m-d H:i:s');

		      	$sql = "INSERT INTO primo_permalinks (created, nsrid, path, version) VALUES (:created, :nsrid, :path, :version)";
			$stmt = $pdo->prepare($sql);

			$stmt->bindValue(':created', $now);
			$stmt->bindValue(':nsrid', $nsrid);
			$stmt->bindValue(':path', $path);
			$stmt->bindValue(':version', $version);

			$inserted = $stmt->execute();

			if (!$inserted) {
				# send error message to admin
			}

			header('Content-Type: application/javascript');
			echo $nsrid;

			exit();	

		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e->getMessage();
		}
	}
}





?>