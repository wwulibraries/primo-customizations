<?php

/* 
David Bass (with help from Lesley Lowery)
Western Washington University
January 2015
https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch
 */

/* define the variables specific to your organization */
$domain = "onesearch.library.wwu.edu";

$scopes = array();
	/*  you can get a list of your scopes by running the following command from your web server's command line (or any computer that has access to the 
ExLibris API):
	$curl http://onesearch.library.wwu.edu/PrimoWebServices/xservice/getscopesofview?viewId=WWU

	Simply replace the viewId with your viewId, and then replace the values below with your values.
	*/

$scopes["atwwu"] = "loc=local,scope:(WWU,wwucedar,E-WWU)";
$scopes["wwusummit"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)";
$scopes["everything"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)&loc=adaptor,primo_central_multiple_fe";
$scopes["everythingandmore"] = "loc=local,scope:(WWU,P,wwucedar,E-WWU)&loc=adaptor,primo_central_multiple_fe";

// there should be one row for each of the scope tabs, and the list of scope values, e.g. (WWU,P,wwucedar,E-WWU) must match the order as they are in the PBO.  see  https://developers.exlibrisgroup.com/primo/apis/webservices/xservices/search/briefsearch for details

/* you probably won't need to tweak anything below this line - - - - - - - - - - - - - - -- - - - - - - - - - - - - - -- - - - - - - - - - - - - - -- - - - - - - - - - - - - - - */

header('Content-Type: application/javascript');

#TODO: determine if the user is logged-in, and pass that to the curl request

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: EXLRequestType, Origin, Content-Type, Accept');
header('Access-Control-Request-Headers: x-requested-with');


// what is the instCode ?
if (!isset($_GET['i'])) {
	exit();
} else {
	$institution =  $_GET['i'];
	$institution = filter_var($institution, FILTER_SANITIZE_STRING);

}

// get the query (what the user searched for)
if (!isset($_GET['q'])) {
	exit();
} else {
	$q = $_GET['q'];
	$query = "";

	// if the query is an array (multiple queries, as created by advanced search), then append them to a string (as needed by the API)
	if (is_array($q)) {
		foreach ($q as $value) {
		    $query .= "&query=" . $value;
		}	
	} else {
	   $query .= "&query=" . $q;
	}

	$query = str_replace(" ", "+", $query);		// replace spaces with the plus symbol

}


// get the scope (which tab we're searching in)
if (!isset($_GET['s'])) {
	exit();
} else {
	$s = $_GET['s'];
	$scope_key = filter_var($s, FILTER_SANITIZE_STRING);
	$scope = $scopes[$s];		// try to find a match in the scopes array; 
	$num_results["count"] = getCount($scope_key, $scope, $query, $institution, $domain);
}



$callback = $_GET['callback'];

if (isset($_GET['callback'])) {
	$callback = $_GET['callback'];
	$callback_no_underscore = str_replace("_", "", $callback);

	# callback should be something like jQuery1830540019340114668_1378922846134
	# to sanitize it, we're going to remove the underscore, and then make sure it's alphanumberic only

	if (!ctype_alnum($callback_no_underscore)) {
		header('status: 400 Bad Request', true, 400);
		exit();
	}

	# TODO: sanitize callback variable - see http://www.geekality.net/2010/06/27/php-how-to-easily-provide-json-and-jsonp/ 
} else {
	echo "missing callback";
	exit();
}


function getCount($scope_key, $scope, $query, $institution, $domain) {
	$expand_results = "";
	if ($scope_key == "everythingandmore") {
		$expand_results = "&pcAvailability=true";
	}

	$url = "http://" . $domain . "/PrimoWebServices/xservice/search/brief?"  . $expand_results . "&json=true&institution=" . $institution . "&onCampus=true&indx=1&bulkSize=1&dym=true&lang=eng&" . $scope . $query;

	//$url = escapeshellcmd($url);

	$curlSession = curl_init();
	curl_setopt($curlSession, CURLOPT_URL, $url);
	curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
	curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($curlSession);
	curl_close($curlSession);
	$json_array = json_decode($data, true);
	#return $data;
	$count = $json_array["SEGMENTS"]["JAGROOT"]["RESULT"]["DOCSET"]["@TOTALHITS"];
	return number_format($count);
}


// var_dump($num_results);



# convert the match arrays into a json object
$json_response = json_encode($num_results);

# return matches as JSON response
echo $callback . "(" . $json_response . ")";

?>