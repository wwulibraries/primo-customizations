
## Synopsis

This repository includes files used to customize the interface of Primo (by Ex Libris) at Western Washington University.

The production URL is http://library.wwu.edu/onesearch, but this version is slightly ahead of production, and is available via
http://onesearch.library.wwu.edu/primo_library/libweb/action/search.do?vid=WWU_David

![screenshot](onesearch-customizations-screenshot.png )

Please note that these customizations require that you use the tabbed scope interface in the PBO; it will not work with the standard select-list scope list.

To use this in your environment, we recommend that you create a test view (using the Primo Back Office, or PBO), and load (or point to) these files.  I prefer to have our header, footer and css files hosted on our website, instead of uploading them to the PBO.  

## Features
1. scope tabs with number of search results
2. facet checkboxes with AND and OR functionality
3. permalinks
4. revised menu system
5. jump to page
6. other minor improvements


## How do I get set up?
* use the form-based Configuration Wizard (http://dev0.library.wwu.edu/primo/features.html), or clone this repository and install the files onto your webserver.
* Summary of set up
	* https://bitbucket.org/wwulibraries/primo-customizations/wiki/%20Scopes%20and%20tabs%20setup%20in%20the%20Primo%20Back%20Office
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Who do I talk to?
* david.bass at wwu dot edu
* lesley.lowery at wwu dot edu

## License
MIT (http://opensource.org/licenses/MIT)

Copyright 2016 WWU
